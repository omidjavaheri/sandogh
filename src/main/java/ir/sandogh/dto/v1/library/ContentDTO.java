package ir.sandogh.dto.v1.library;

import ir.sandogh.dto.v1.PageableDTO;
import ir.sandogh.entities.library.Content;

public class ContentDTO {
    private Content content;
    private PageableDTO pageableDTO;

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
