package ir.sandogh.dto.v1.shop;

import ir.sandogh.entities.shop.ProductState;

public class UpdateStateDTO {
    private Long id;
    private ProductState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductState getState() {
        return state;
    }

    public void setState(ProductState state) {
        this.state = state;
    }
}
