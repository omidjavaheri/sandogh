package ir.sandogh.dto.v1.insurance.payment;

public class AuthorityDTO {
    private String value;

    public AuthorityDTO() {
    }

    public AuthorityDTO(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
