package ir.sandogh.dto.v1.insurance.policy;

import ir.sandogh.entities.insurance.policy.Agent;

public class AgentCounterDTO {

    private Agent agent;
    private Long policyCount;

    public void plusOne(){
        this.policyCount++;
    }

    public AgentCounterDTO() {
    }

    public AgentCounterDTO(Agent username, Long policyCount) {
        this.agent = username;
        this.policyCount = policyCount;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Long getPolicyCount() {
        return policyCount;
    }

    public void setPolicyCount(Long policyCount) {
        this.policyCount = policyCount;
    }
}
