package ir.sandogh.dto.v1.insurance.policy;

import ir.sandogh.dto.v1.PageableDTO;
import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;

public class SupervisorFetchPoliciesDTO {

    private Insurer insurer;

    private PageableDTO pageableDTO;

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
