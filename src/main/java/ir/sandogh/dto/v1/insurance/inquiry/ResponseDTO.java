package ir.sandogh.dto.v1.insurance.inquiry;

public class ResponseDTO {

    private Long id;
    private Double value;

    public ResponseDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
