package ir.sandogh.dto.v1.insurance.inquiry;

public class ThirdPartyDTO {

    private Long vehicleKind;
    private int nonDamageDiscount;
    private double latencyInDays;
    private int physicalDamage;
    private int financialDamage;
    private Long vehicleGroup;
    private int passengerNonDamageDiscount;
    private int passengerDamage;
    private double coverageAmount;
    private Long vehicleUsage;

    public Long getVehicleKind() {
        return vehicleKind;
    }

    public void setVehicleKind(Long vehicleKind) {
        this.vehicleKind = vehicleKind;
    }

    public int getNonDamageDiscount() {
        return nonDamageDiscount;
    }

    public void setNonDamageDiscount(int nonDamageDiscount) {
        this.nonDamageDiscount = nonDamageDiscount;
    }

    public double getLatencyInDays() {
        return latencyInDays;
    }

    public void setLatencyInDays(double latencyInDays) {
        this.latencyInDays = latencyInDays;
    }

    public int getPhysicalDamage() {
        return physicalDamage;
    }

    public void setPhysicalDamage(int physicalDamage) {
        this.physicalDamage = physicalDamage;
    }

    public int getFinancialDamage() {
        return financialDamage;
    }

    public void setFinancialDamage(int financialDamage) {
        this.financialDamage = financialDamage;
    }

    public Long getVehicleGroup() {
        return vehicleGroup;
    }

    public void setVehicleGroup(Long vehicleGroup) {
        this.vehicleGroup = vehicleGroup;
    }

    public int getPassengerNonDamageDiscount() {
        return passengerNonDamageDiscount;
    }

    public void setPassengerNonDamageDiscount(int passengerNonDamageDiscount) {
        this.passengerNonDamageDiscount = passengerNonDamageDiscount;
    }

    public int getPassengerDamage() {
        return passengerDamage;
    }

    public void setPassengerDamage(int passengerDamage) {
        this.passengerDamage = passengerDamage;
    }

    public double getCoverageAmount() {
        return coverageAmount;
    }

    public void setCoverageAmount(double coverageAmount) {
        this.coverageAmount = coverageAmount;
    }

    public Long getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(Long vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    @Override
    public String toString() {
        return "ShakhsSalesDTO{" +
                "vehicleKind='" + vehicleKind + '\'' +
                ", nonDamageDiscount=" + nonDamageDiscount +
                ", latencyInDays=" + latencyInDays +
                ", physicalDamage=" + physicalDamage +
                ", financialDamage=" + financialDamage +
                ", vehicleGroup='" + vehicleGroup + '\'' +
                ", passengerNonDamageDiscount=" + passengerNonDamageDiscount +
                ", passengerDamage=" + passengerDamage +
                ", coverageAmount=" + coverageAmount +
                ", vehicleUsage='" + vehicleUsage + '\'' +
                '}';
    }
}
