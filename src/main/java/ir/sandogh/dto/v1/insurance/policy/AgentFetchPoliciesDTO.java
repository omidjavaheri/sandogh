package ir.sandogh.dto.v1.insurance.policy;

import ir.sandogh.dto.v1.PageableDTO;

public class AgentFetchPoliciesDTO {

    private PageableDTO pageableDTO;

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
