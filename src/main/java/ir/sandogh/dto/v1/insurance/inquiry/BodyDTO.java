package ir.sandogh.dto.v1.insurance.inquiry;

public class BodyDTO {

    private double vehicleValue;
    private Long vehicleGroup;
    private Long vehicleUsage;
    private int age;
    private boolean additionalCover_breakingGlass;
    private boolean additionalCover_theft;
    private boolean additionalCover_naturalDisaster;
    private boolean additionalCover_transportationFee;
    private boolean additionalCover_priceFluctuation;
    private boolean discount_agreedOn;
    private boolean discount_coverage;
    private boolean discount_cash;

    public double getVehicleValue() {
        return vehicleValue;
    }

    public void setVehicleValue(double vehicleValue) {
        this.vehicleValue = vehicleValue;
    }

    public Long getVehicleGroup() {
        return vehicleGroup;
    }

    public void setVehicleGroup(Long vehicleGroup) {
        this.vehicleGroup = vehicleGroup;
    }

    public Long getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(Long vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isAdditionalCover_breakingGlass() {
        return additionalCover_breakingGlass;
    }

    public void setAdditionalCover_breakingGlass(boolean additionalCover_breakingGlass) {
        this.additionalCover_breakingGlass = additionalCover_breakingGlass;
    }

    public boolean isAdditionalCover_theft() {
        return additionalCover_theft;
    }

    public void setAdditionalCover_theft(boolean additionalCover_theft) {
        this.additionalCover_theft = additionalCover_theft;
    }

    public boolean isAdditionalCover_naturalDisaster() {
        return additionalCover_naturalDisaster;
    }

    public void setAdditionalCover_naturalDisaster(boolean additionalCover_naturalDisaster) {
        this.additionalCover_naturalDisaster = additionalCover_naturalDisaster;
    }

    public boolean isAdditionalCover_transportationFee() {
        return additionalCover_transportationFee;
    }

    public void setAdditionalCover_transportationFee(boolean additionalCover_transportationFee) {
        this.additionalCover_transportationFee = additionalCover_transportationFee;
    }

    public boolean isAdditionalCover_priceFluctuation() {
        return additionalCover_priceFluctuation;
    }

    public void setAdditionalCover_priceFluctuation(boolean additionalCover_priceFluctuation) {
        this.additionalCover_priceFluctuation = additionalCover_priceFluctuation;
    }

    public boolean isDiscount_agreedOn() {
        return discount_agreedOn;
    }

    public void setDiscount_agreedOn(boolean discount_agreedOn) {
        this.discount_agreedOn = discount_agreedOn;
    }

    public boolean isDiscount_coverage() {
        return discount_coverage;
    }

    public void setDiscount_coverage(boolean discount_coverage) {
        this.discount_coverage = discount_coverage;
    }

    public boolean isDiscount_cash() {
        return discount_cash;
    }

    public void setDiscount_cash(boolean discount_cash) {
        this.discount_cash = discount_cash;
    }

    @Override
    public String toString() {
        return "BadaneDTO{" +
                "vehicleValue=" + vehicleValue +
                ", vehicleGroup='" + vehicleGroup + '\'' +
                ", vehicleUsage='" + vehicleUsage + '\'' +
                ", age=" + age +
                ", additionalCover_breakingGlass=" + additionalCover_breakingGlass +
                ", additionalCover_theft=" + additionalCover_theft +
                ", additionalCover_naturalDisaster=" + additionalCover_naturalDisaster +
                ", additionalCover_transportationFee=" + additionalCover_transportationFee +
                ", additionalCover_priceFluctuation=" + additionalCover_priceFluctuation +
                ", discount_agreedOn=" + discount_agreedOn +
                ", discount_coverage=" + discount_coverage +
                ", discount_cash=" + discount_cash +
                '}';
    }
}
