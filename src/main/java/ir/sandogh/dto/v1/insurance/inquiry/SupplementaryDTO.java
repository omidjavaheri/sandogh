package ir.sandogh.dto.v1.insurance.inquiry;

public class SupplementaryDTO {

    private int ageGroup;
    private boolean basicInsurer;

    public int getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(int ageGroup) {
        this.ageGroup = ageGroup;
    }

    public boolean isBasicInsurer() {
        return basicInsurer;
    }

    public void setBasicInsurer(boolean basicInsurer) {
        this.basicInsurer = basicInsurer;
    }

    @Override
    public String toString() {
        return "TakmiliDTO{" +
                "ageGroup=" + ageGroup +
                ", basicInsurer=" + basicInsurer +
                '}';
    }
}
