package ir.sandogh.dto.v1.insurance.inquiry;

import ir.sandogh.entities.insurance.inquiry.supplementary.PlanType;

public class SupplementaryResponseDTO {

    private Long insurerId;

    private PlanType plan;

    private Double cost;

    public SupplementaryResponseDTO() {
    }

    public Long getInsurerId() {
        return insurerId;
    }

    public void setInsurerId(Long insurerId) {
        this.insurerId = insurerId;
    }

    public PlanType getPlan() {
        return plan;
    }

    public void setPlan(PlanType plan) {
        this.plan = plan;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }
}
