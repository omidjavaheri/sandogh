package ir.sandogh.dto.v1.insurance.policy;

import java.util.List;

public class SupervisorChangePoliciesAgentDTO {
    private List<Long> ids;
    private String targetAgentUsername;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public String getTargetAgentUsername() {
        return targetAgentUsername;
    }

    public void setTargetAgentUsername(String targetAgentUsername) {
        this.targetAgentUsername = targetAgentUsername;
    }
}
