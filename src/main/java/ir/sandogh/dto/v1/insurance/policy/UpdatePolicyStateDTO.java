package ir.sandogh.dto.v1.insurance.policy;

import ir.sandogh.entities.insurance.policy.PolicyState;

public class UpdatePolicyStateDTO {

    private Long id;
    private PolicyState state;
    private String agentUsername;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PolicyState getState() {
        return state;
    }

    public void setState(PolicyState state) {
        this.state = state;
    }

    public String getAgentUsername() {
        return agentUsername;
    }

    public void setAgentUsername(String agentUsername) {
        this.agentUsername = agentUsername;
    }
}
