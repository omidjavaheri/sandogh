package ir.sandogh.dto.v1.insurance.policy;

import ir.sandogh.entities.insurance.policy.Agency;

public class AgencyCounterDTO {

    private Agency agency;
    private Long policyCount;

    public AgencyCounterDTO() {
    }

    public void plusOne(){
        this.policyCount++;
    }

    public AgencyCounterDTO(Agency username, Long policyCount) {
        this.agency = username;
        this.policyCount = policyCount;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public Long getPolicyCount() {
        return policyCount;
    }

    public void setPolicyCount(Long policyCount) {
        this.policyCount = policyCount;
    }
}
