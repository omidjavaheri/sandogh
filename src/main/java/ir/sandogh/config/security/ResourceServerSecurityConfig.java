package ir.sandogh.config.security;

import ir.sandogh.entities.Role;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerSecurityConfig extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "resource_id";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).stateless(false);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http/*.
                 anonymous().disable()*/
                .authorizeRequests()
                .antMatchers("/actuator/**").hasAuthority(Role.ACTUXXATOR_ADMXXIN.getServer())
                .antMatchers("/**/facilities/**").hasAuthority(Role.CUSTXXOMER.getServer())
                .antMatchers("/**/admin/**").hasAuthority(Role.ADXXMIN.getServer())
                .antMatchers("/**/insurance/agent/**").hasAuthority(Role.AGEXXNT.getServer())
                .antMatchers("/**/insurance/supervisor/**").hasAuthority(Role.SUPERXXVISOR.getServer())
                .antMatchers("/**/shop/club/**").hasAuthority(Role.CLXXUB_ADXXMIN.getServer())
                .antMatchers("/**/shop/organization/**").hasAuthority(Role.ORGANIZXXATION_ADXXMIN.getServer())
                .antMatchers("/**/shop/owner/**").hasAuthority(Role.SHXXOP_OWXXNER.getServer())
                .antMatchers("/**/user/authenticated/**").permitAll()
                .antMatchers("/**/user/anonymous/**").anonymous()
                .antMatchers("/**/payment/request").hasAuthority(Role.CUSTXXOMER.getServer())
                .antMatchers("/**/payment/callback").anonymous()
                .anyRequest().denyAll()
                .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }
}
