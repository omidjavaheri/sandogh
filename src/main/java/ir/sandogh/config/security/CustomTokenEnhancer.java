package ir.sandogh.config.security;

import ir.sandogh.entities.Role;
import ir.sandogh.entities.User;
import ir.sandogh.entities.insurance.policy.Agent;
import ir.sandogh.services.insurance.policy.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Autowired
    private AgentService agentService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        final Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put("user", user.convertEntityToDTO());
        if (user.getRole().equals(Role.AGEXXNT.getServer()) || user.getRole().equals(Role.SUPERXXVISOR.getServer())) {
            Agent agent = agentService.findByUsername(user.getUsername());
            additionalInfo.put("agencyId", agent.getAgency().getId());
            additionalInfo.put("insurerId", agent.getAgency().getInsurer().getId());
            additionalInfo.put("supervisor", agent.isSupervisor());
        }
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
