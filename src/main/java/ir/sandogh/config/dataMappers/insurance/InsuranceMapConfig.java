package ir.sandogh.config.dataMappers.insurance;


import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.entities.insurance.inquiry.lookup.VehicleGroup;
import ir.sandogh.services.insurance.inquiry.lookup.BloodMoneyService;
import ir.sandogh.services.insurance.inquiry.lookup.InsurerService;
import ir.sandogh.services.insurance.inquiry.lookup.TaxRateService;
import ir.sandogh.services.insurance.inquiry.lookup.VehicleGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class InsuranceMapConfig {

    @Autowired
    BloodMoneyService bloodMoneyService;

    @Autowired
    TaxRateService taxRateService;

    @Autowired
    InsurerService insurerService;

    @Autowired
    VehicleGroupService vehicleGroupService;

    @Bean
    public Iterable<Insurer> allInsurer() {
        return insurerService.findAll();
    }

    @Bean
    public Iterable<VehicleGroup> allVehicleGroup() {
        return vehicleGroupService.findAll();
    }

    @Bean
    public Map<Long, Insurer> id_insurerOBJ() {
        final Map<Long, Insurer> id_insurerOBJ = new HashMap<>();
        for (Insurer insurer : insurerService.findAll()) {
            id_insurerOBJ.put(insurer.getId(), insurer);
        }
        return id_insurerOBJ;
    }

    @Bean
    public double bloodMoney() {
        return bloodMoneyService.findAll().iterator().next().getValue();
    }

    @Bean
    public double taxRate() {
        return taxRateService.findAll().iterator().next().getRate();
    }
}
