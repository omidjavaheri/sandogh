package ir.sandogh.config.dataMappers.insurance;


import ir.sandogh.entities.insurance.inquiry.thirdParty.*;
import ir.sandogh.services.insurance.inquiry.lookup.VehicleKindService;
import ir.sandogh.services.insurance.inquiry.thirdParty.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ThirdPartyMapConfig {

    @Autowired
    DriverAccidentRateService driverAccidentRateService;

    @Autowired
    BasicPremiumService basicPremiumService;

    @Autowired
    FinancialSurplusService financialSurplusService;

    @Autowired
    UsageRateService usageRateService;

    @Autowired
    InsurerDiscountService insurerDiscountService;


    @Autowired
    VehicleKindService vehicleKindService;

    @Bean
    public Map<String, Double> insurer_vehicleKind_basicPremium() {
        final Map<String, Double> insurer_vehicleKind_basicPremium = new HashMap<>();
        for (BasicPremium a : basicPremiumService.findAll()) {
            String key = a.getVehicleKind().getInsurer().getId() + "-" +
                    a.getVehicleKind().getType().getId();
            insurer_vehicleKind_basicPremium.put(key, a.getBasicPremium());
        }
        return insurer_vehicleKind_basicPremium;
    }

    @Bean
    public Map<String, Double> insurer_vehicleGroup_driveAccidentRate() {
        final Map<String, Double> insurer_vehicleGroupe_driveAccidentRate = new HashMap<>();
        for (DriverAccidentRate a : driverAccidentRateService.findAll()) {
            String key = a.getVehicleGroup().getInsurer().getId() + "-" +
                    a.getVehicleGroup().getType().getId();
            insurer_vehicleGroupe_driveAccidentRate.put(key, a.getRate());
        }
        return insurer_vehicleGroupe_driveAccidentRate;
    }


    @Bean
    public Map<String, Double> insurer_usageType_increaseRate() {
        final Map<String, Double> insurer_usageType_increaseRate = new HashMap<>();
        for (UsageRate a : usageRateService.findAll()) {
            String key = a.getVehicleUsage().getVehicleGroup().getInsurer().getId() + "-" +
                    a.getVehicleUsage().getType().getId();
            insurer_usageType_increaseRate.put(key, a.getRate());
        }
        return insurer_usageType_increaseRate;
    }

    @Bean
    public Map<Long, Double> insurer_discount() {
        final Map<Long, Double> insurer_discount = new HashMap<>();
        Iterable<InsurerDiscount> all = insurerDiscountService.findAll();
        for (InsurerDiscount a : all) {
            insurer_discount.put(a.getInsurer().getId(), a.getDiscount());
        }
        return insurer_discount;
    }

    @Bean
    public Map<String, Double> insurer_vehicleKind_coverageAmount_financialSurplus() {
        final Map<String, Double> insurer_vehicleKind_coverageAmount_financialSurplus = new HashMap<>();
        for (FinancialSurplus a : financialSurplusService.findAll()) {
            String key = a.getVehicleKind().getInsurer().getId() + "-" +
                    a.getVehicleKind().getType().getId() + "-" +
                    a.getCoverageAmount();
            insurer_vehicleKind_coverageAmount_financialSurplus.put(key, a.getFinancialSurplus());
        }
        return insurer_vehicleKind_coverageAmount_financialSurplus;
    }
}
