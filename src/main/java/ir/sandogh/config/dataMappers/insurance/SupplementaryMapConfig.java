package ir.sandogh.config.dataMappers.insurance;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.entities.insurance.inquiry.supplementary.BasicInsurerRate;
import ir.sandogh.entities.insurance.inquiry.supplementary.SupplementaryPremium;
import ir.sandogh.services.insurance.inquiry.supplementary.BasicInsurerRateService;
import ir.sandogh.services.insurance.inquiry.supplementary.SupplementaryPremiumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class SupplementaryMapConfig {

    @Autowired
    BasicInsurerRateService basicInsurerRateService;

    @Autowired
    SupplementaryPremiumService supplementaryPremiumService;

    @Resource(name = "allInsurer")
    private Iterable<Insurer> allBimegar;

    @Bean
    public Map<String, Iterable<SupplementaryPremium>> insurer_ageGroup_plan_cost() {
        final Map<String, Iterable<SupplementaryPremium>> insurer_ageGroup_plan_cost = new HashMap<>();
        for (Insurer insurer : allBimegar) {
            for (Integer ageGroup : supplementaryPremiumService.findDistinctAgeGroup()) {
                Iterable<SupplementaryPremium> all = supplementaryPremiumService.findByBimegarAndAgeGroup(insurer, ageGroup);
                String key = insurer.getId() + "-" + ageGroup;
                insurer_ageGroup_plan_cost.put(key, all);
            }
        }
        return insurer_ageGroup_plan_cost;
    }

    @Bean
    public Map<Long, Double> insurer_basicInsurerRate() {
        final Map<Long, Double> insurer_basicInsurerRate = new HashMap<>();
        Iterable<BasicInsurerRate> all = basicInsurerRateService.findAll();
        for (BasicInsurerRate a : all) {
            insurer_basicInsurerRate.put(a.getInsurer().getId(), a.getIncreaseRate());
        }
        return insurer_basicInsurerRate;
    }
}
