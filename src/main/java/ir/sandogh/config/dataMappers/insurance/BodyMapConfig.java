package ir.sandogh.config.dataMappers.insurance;

import ir.sandogh.entities.insurance.inquiry.body.AdditionalCoverRate;
import ir.sandogh.entities.insurance.inquiry.body.AgeRate;
import ir.sandogh.entities.insurance.inquiry.body.BodyRate;
import ir.sandogh.entities.insurance.inquiry.body.DiscountRate;
import ir.sandogh.services.insurance.inquiry.body.AdditionalCoverRateService;
import ir.sandogh.services.insurance.inquiry.body.AgeRateService;
import ir.sandogh.services.insurance.inquiry.body.BodyRateService;
import ir.sandogh.services.insurance.inquiry.body.DiscountRateService;
import ir.sandogh.services.insurance.inquiry.lookup.VehicleGroupService;
import ir.sandogh.services.insurance.inquiry.lookup.VehicleUsageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class BodyMapConfig {

    @Autowired
    BodyRateService bodyRateService;

    @Autowired
    AgeRateService ageRateService;

    @Autowired
    AdditionalCoverRateService additionalCoverRateService;

    @Autowired
    VehicleGroupService vehicleGroupService;

    @Autowired
    VehicleUsageService vehicleUsageService;

    @Autowired
    DiscountRateService discountRateService;

    @Bean
    public Map<Long, Double> insurer_ageEachYearRate() {
        final Map<Long, Double> insurer_ageEachYearRate = new HashMap<>();
        Iterable<AgeRate> all = ageRateService.findAll();
        for (AgeRate a : all) {
            insurer_ageEachYearRate.put(a.getInsurer().getId(), a.getEachYearRate());
        }
        return insurer_ageEachYearRate;
    }

    @Bean
    public Map<Long, Integer> insurer_ageStartingYear() {
        final Map<Long, Integer> insurer_ageStartingYear = new HashMap<>();
        Iterable<AgeRate> all = ageRateService.findAll();
        for (AgeRate a : all) {
            insurer_ageStartingYear.put(a.getInsurer().getId(), a.getStartingYear());
        }
        return insurer_ageStartingYear;
    }

    @Bean
    public Map<String, Double> insurer_discountType_discountRate() {
        final Map<String, Double> insurer_discountType_discountRate = new HashMap<>();
        for (DiscountRate a : discountRateService.findAll()) {
            String key = a.getInsurer().getId() + "-" + a.getType().getId();
            insurer_discountType_discountRate.put(key, a.getRate());
        }
        return insurer_discountType_discountRate;
    }

    @Bean
    public Map<String, Double> insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate() {
        final Map<String, Double> insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate = new HashMap<>();
        for (AdditionalCoverRate a : additionalCoverRateService.findAll()) {
            String key = a.getVehicleUsage().getVehicleGroup().getInsurer().getId() + "-" +
                    a.getVehicleUsage().getVehicleGroup().getType().getId() + "-" +
                    a.getVehicleUsage().getType().getId() + "-" +
                    a.getType().getId();
            insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate.put(key, a.getRate());
        }
        return insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate;
    }

    @Bean
    public Map<String, Double> insurer_vehicleGroup_vehicleUsage_bodyRate() {
        final Map<String, Double> insurer_vehicleGroup_vehicleUsage_bodyRate = new HashMap<>();
        for (BodyRate a : bodyRateService.findAll()) {
            String key = a.getVehicleUsage().getVehicleGroup().getInsurer().getId() + "-" +
                    a.getVehicleUsage().getVehicleGroup().getType().getId() + "-" +
                    a.getVehicleUsage().getType().getId();
            insurer_vehicleGroup_vehicleUsage_bodyRate.put(key, a.getRate());
        }
        return insurer_vehicleGroup_vehicleUsage_bodyRate;
    }
}
