package ir.sandogh.config.dataMappers.insurance;

import ir.sandogh.dto.v1.insurance.policy.AgencyCounterDTO;
import ir.sandogh.dto.v1.insurance.policy.AgentCounterDTO;
import ir.sandogh.entities.insurance.policy.Agency;
import ir.sandogh.entities.insurance.policy.Agent;
import ir.sandogh.repositories.insurance.policy.PolicyRepository;
import ir.sandogh.services.insurance.policy.AgencyService;
import ir.sandogh.services.insurance.policy.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class RegistryMapConfig {

    @Autowired
    AgencyService agencyService;

    @Autowired
    AgentService agentService;

    @Autowired
    PolicyRepository policyRepository;

    @Bean
    public Map<Long, List<AgencyCounterDTO>> insurer_agencyPOJOList() {
        final Map<Long, List<AgencyCounterDTO>> insurer_agencyPOJOList = new HashMap<>();
        for (Agency a : agencyService.findAll()) {
            Long key = a.getInsurer().getId();
            if (insurer_agencyPOJOList.containsKey(key)) {
                insurer_agencyPOJOList.get(key).add(new AgencyCounterDTO(a, policyRepository.countByAgency(a)));
            } else {
                List<AgencyCounterDTO> temp = new ArrayList<>();
                temp.add(new AgencyCounterDTO(a, policyRepository.countByAgency(a)));
                insurer_agencyPOJOList.put(key, temp);
            }
        }
        return insurer_agencyPOJOList;
    }

    @Bean
    public Map<Long, List<AgentCounterDTO>> agency_agentPOJOList() {
        final Map<Long, List<AgentCounterDTO>> agency_agentPOJOList = new HashMap<>();
        for (Agent a : agentService.findAll()) {
            Long key = a.getAgency().getId();
            if(!a.isSupervisor()){
                if (agency_agentPOJOList.containsKey(key)) {
                    agency_agentPOJOList.get(key).add(new AgentCounterDTO(a, policyRepository.countByAssignedAgentUsername(a.getUsername())));
                } else {
                    List<AgentCounterDTO> temp = new ArrayList<>();
                    temp.add(new AgentCounterDTO(a, policyRepository.countByAssignedAgentUsername(a.getUsername())));
                    agency_agentPOJOList.put(key, temp);
                }
            }
        }
        return agency_agentPOJOList;
    }
}
