package ir.sandogh.controllers;

import ir.sandogh.dto.v1.library.ContentDTO;
import ir.sandogh.dto.v1.shop.UpdateStateDTO;
import ir.sandogh.entities.library.Content;
import ir.sandogh.entities.shop.Product;
import ir.sandogh.entities.shop.ProductState;
import ir.sandogh.services.library.ContentService;
import ir.sandogh.services.shop.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/admin")
public class AdminController {

    @Autowired
    private ContentService contentService;
    @Autowired
    private ShopService shopService;

    @PostMapping("/library/save")
    @ResponseBody
    public void register(@RequestBody Content dto) {
        contentService.save(dto);
    }

    @PostMapping("/library/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        contentService.delete(id);
    }

    @PostMapping("/library/find")
    @ResponseBody
    public List<Content> find(@RequestBody ContentDTO dto) {
        return contentService.findByTypeAndField(dto);
    }

    @PostMapping("/shop/find")
    @ResponseBody
    public Iterable<Product> shopProductFind() {
        return shopService.findAll(ProductState.CLUB_APPROVAL);
    }

    @PostMapping("/shop/updateState")
    @ResponseBody
    public Product productUpdateState(@RequestBody UpdateStateDTO dto) {
        return shopService.updateSate(dto);
    }

}
