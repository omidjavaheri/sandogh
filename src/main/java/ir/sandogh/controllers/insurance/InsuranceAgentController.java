package ir.sandogh.controllers.insurance;

import ir.sandogh.dto.v1.PageableDTO;
import ir.sandogh.dto.v1.insurance.policy.UpdatePolicyStateDTO;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.services.insurance.policy.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/insurance/agent")
public class InsuranceAgentController {

    @Autowired
    private PolicyService policyService;

    @PostMapping("/find")
    @ResponseBody
    public List<Policy> agentPoliciesFind(@RequestBody PageableDTO dto) {
        return policyService.agentFindPolicies(dto);
    }

    @PostMapping("/changeState")
    @ResponseBody
    public Policy updatePolicyState(@RequestBody UpdatePolicyStateDTO dto) {
        return policyService.updatePolicyState(dto);
    }
}
