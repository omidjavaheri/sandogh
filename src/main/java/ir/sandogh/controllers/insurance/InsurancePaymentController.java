package ir.sandogh.controllers.insurance;

import ir.sandogh.dto.v1.insurance.payment.AuthorityDTO;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.services.insurance.InsurancePaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * this controller handles policies with payment
 */
@RestController
@RequestMapping("/v1/insurance/payment")
public class InsurancePaymentController {
    @Autowired
    private InsurancePaymentService insurancePaymentService;

    @PostMapping(value = "/request")
    @ResponseBody
    public AuthorityDTO paymentRequest(@RequestBody Policy policy) {
        return insurancePaymentService.paymentRequest(policy);
    }

    @GetMapping("/callback")
    @ResponseBody
    public void callback(HttpServletResponse response, @RequestParam("Authority") String authority, @RequestParam("Status") String status) {
        insurancePaymentService.callback(response, authority, status);
    }
}
