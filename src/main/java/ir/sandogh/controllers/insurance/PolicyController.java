package ir.sandogh.controllers.insurance;

import ir.sandogh.dto.v1.insurance.inquiry.*;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.services.insurance.inquiry.body.BodyInquiryService;
import ir.sandogh.services.insurance.inquiry.supplementary.SupplementaryInquiryService;
import ir.sandogh.services.insurance.inquiry.thirdParty.ThirdPartyInquiryService;
import ir.sandogh.services.insurance.policy.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/v1/facilities/insurance")
public class PolicyController {

    @Autowired
    private PolicyService policyService;
    @Autowired
    SupplementaryInquiryService supplementaryInquiryService;
    @Autowired
    ThirdPartyInquiryService thirdPartyInquiryService;
    @Autowired
    BodyInquiryService bodyInquiryService;

    /**
     * this endpoint handles policies without payment
     */
    @PostMapping("/registry")
    @ResponseBody
    public void register(@RequestBody Policy policy) {
         policyService.saveNonPaymentPolicySave(policy);
    }

    @PostMapping("/supplementaryInquiry")
    @ResponseBody
    public List<SupplementaryResponseDTO> supplementaryInquiry(@RequestBody SupplementaryDTO dto) {
        return supplementaryInquiryService.inquiry(dto);
    }

    @PostMapping("/thirdPartyInquiry")
    @ResponseBody
    public List<ResponseDTO> thirdPartyInquiry(@RequestBody ThirdPartyDTO dto) {
        return thirdPartyInquiryService.inquiry(dto);
    }

    @PostMapping("/bodyInquiry")
    @ResponseBody
    public List<ResponseDTO> bodyInquiry(@RequestBody BodyDTO dto) {
        return bodyInquiryService.inquiry(dto);
    }
}
