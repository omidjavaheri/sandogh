package ir.sandogh.controllers.insurance;

import ir.sandogh.dto.v1.insurance.policy.SupervisorChangePoliciesAgentDTO;
import ir.sandogh.dto.v1.insurance.policy.SupervisorFetchPoliciesDTO;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.services.insurance.policy.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/insurance/supervisor")
public class InsuranceSupervisorController {

    @Autowired
    private PolicyService policyService;

    @PostMapping("/find")
    @ResponseBody
    public List<Policy> supervisorApplicationGet(@RequestBody SupervisorFetchPoliciesDTO dto) {
        return policyService.supervisorGetApplications(dto);
    }

    @PostMapping("/changeAgent")
    @ResponseBody
    public List<Long> supervisorApplicationGet(@RequestBody SupervisorChangePoliciesAgentDTO dto) {
        return policyService.supervisorChangePoliciesAgent(dto);
    }
}
