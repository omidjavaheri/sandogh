package ir.sandogh.controllers.library;

import ir.sandogh.dto.v1.library.ContentDTO;
import ir.sandogh.entities.library.Content;
import ir.sandogh.services.library.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/facilities/library")
public class LibraryController {

    @Autowired
    private ContentService contentService;

    @PostMapping("/find")
    @ResponseBody
    public List<Content> find(@RequestBody ContentDTO dto) {
        return contentService.findByTypeAndField(dto);
    }

}
