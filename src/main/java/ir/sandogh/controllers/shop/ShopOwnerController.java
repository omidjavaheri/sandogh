package ir.sandogh.controllers.shop;

import ir.sandogh.entities.shop.Product;
import ir.sandogh.entities.shop.ProductState;
import ir.sandogh.services.shop.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/shop/owner")
public class ShopOwnerController {

    @Autowired
    private ShopService shopService;

    @PostMapping("/register")
    @ResponseBody
    public Product registerProduct(@RequestBody Product product) {
        product.setState(ProductState.INITIAL_REGISTRATION);
        return shopService.save(product);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Product> shopOwnerFind() {
        return shopService.findAllByShopOwnerUsername();
    }

    @PostMapping("/delete")
    @ResponseBody
    public void shopOwnerDelete(@RequestBody Long id) {
        shopService.deleteById(id);
    }
}
