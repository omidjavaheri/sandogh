package ir.sandogh.controllers.shop;

import ir.sandogh.entities.shop.Product;
import ir.sandogh.entities.shop.ProductState;
import ir.sandogh.services.shop.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/facilities/shop/product")
public class ShopProductController {
    @Autowired
    private ShopService shopService;

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Product> userProductFind() {
        return shopService.findAll(ProductState.SHOP_APPROVAL);
    }

}
