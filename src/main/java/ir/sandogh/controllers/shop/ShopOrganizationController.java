package ir.sandogh.controllers.shop;

import ir.sandogh.dto.v1.shop.UpdateStateDTO;
import ir.sandogh.entities.shop.Product;
import ir.sandogh.entities.shop.ProductState;
import ir.sandogh.services.shop.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/shop/organization")
public class ShopOrganizationController {

    @Autowired
    private ShopService shopService;

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Product> organizationProductFind() {
        return shopService.findAll(ProductState.INITIAL_REGISTRATION);
    }

    @PostMapping("/updateState")
    @ResponseBody
    public Product productUpdateState(@RequestBody UpdateStateDTO dto) {
        return shopService.updateSate(dto);
    }
}
