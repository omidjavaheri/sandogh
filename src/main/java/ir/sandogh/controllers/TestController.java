package ir.sandogh.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;

@RestController
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AuthorizationServerTokenServices tokenServices;

    public String getpass(String pass) {
        return passwordEncoder.encode(pass);
    }

    @GetMapping("/test")
    public String getPo() {
        logger.debug("debug log called for /test &&");
        logger.error("error log called for /test &&");
        ArrayList<Integer> a = new ArrayList<>();
        //a.get(0);
        return "test";
    }

//    public ResponseEntity getrole() {
////        String targetURL = "http://localhost:9000/oauth/token";
////        RestTemplate rt = new RestTemplate();
////        HttpHeaders headers = new HttpHeaders();
////        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
////        headers.add("authorization", "Basic ZW5nX2FwcDplbmdfYXBw");
////        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
////        params.add("grant_type", "password");
////        params.add("username", "karbar");
////        params.add("password", "123456");
////        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
////        ResponseEntity<String> response = rt.postForEntity(targetURL, entity, String.class);
////        return response;
//
//    }


}
