package ir.sandogh.partners.payment.com.zarinpal;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MerchantID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Authority" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ExpireIn" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "merchantID",
        "authority",
        "expireIn"
})
@XmlRootElement(name = "RefreshAuthority")
public class RefreshAuthority {

    @XmlElement(name = "MerchantID", required = true)
    protected String merchantID;
    @XmlElement(name = "Authority", required = true)
    protected String authority;
    @XmlElement(name = "ExpireIn")
    protected int expireIn;

    /**
     * Gets the value of the merchantID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMerchantID() {
        return merchantID;
    }

    /**
     * Sets the value of the merchantID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMerchantID(String value) {
        this.merchantID = value;
    }

    /**
     * Gets the value of the authority property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAuthority() {
        return authority;
    }

    /**
     * Sets the value of the authority property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAuthority(String value) {
        this.authority = value;
    }

    /**
     * Gets the value of the expireIn property.
     */
    public int getExpireIn() {
        return expireIn;
    }

    /**
     * Sets the value of the expireIn property.
     */
    public void setExpireIn(int value) {
        this.expireIn = value;
    }

}
