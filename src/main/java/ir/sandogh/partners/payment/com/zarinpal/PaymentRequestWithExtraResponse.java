package ir.sandogh.partners.payment.com.zarinpal;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Authority" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "status",
        "authority"
})
@XmlRootElement(name = "PaymentRequestWithExtraResponse")
public class PaymentRequestWithExtraResponse {

    @XmlElement(name = "Status")
    protected int status;
    @XmlElement(name = "Authority", required = true)
    protected String authority;

    /**
     * Gets the value of the status property.
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Gets the value of the authority property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAuthority() {
        return authority;
    }

    /**
     * Sets the value of the authority property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAuthority(String value) {
        this.authority = value;
    }

}
