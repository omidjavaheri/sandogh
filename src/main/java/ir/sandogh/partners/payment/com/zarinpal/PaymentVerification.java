package ir.sandogh.partners.payment.com.zarinpal;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MerchantID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Authority" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "merchantID",
        "authority",
        "amount"
})
@XmlRootElement(name = "PaymentVerification")
public class PaymentVerification {

    @XmlElement(name = "MerchantID", required = true)
    protected String merchantID;
    @XmlElement(name = "Authority", required = true)
    protected String authority;
    @XmlElement(name = "Amount")
    protected int amount;

    /**
     * Gets the value of the merchantID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getMerchantID() {
        return merchantID;
    }

    /**
     * Sets the value of the merchantID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setMerchantID(String value) {
        this.merchantID = value;
    }

    /**
     * Gets the value of the authority property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAuthority() {
        return authority;
    }

    /**
     * Sets the value of the authority property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAuthority(String value) {
        this.authority = value;
    }

    /**
     * Gets the value of the amount property.
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     */
    public void setAmount(int value) {
        this.amount = value;
    }

}
