package ir.sandogh.partners.payment.com.zarinpal;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.3.3
 * 2019-09-25T02:33:53.188+03:30
 * Generated source version: 3.3.3
 */
@WebService(targetNamespace = "http://zarinpal.com/", name = "PaymentGatewayImplementationServicePortType")
@XmlSeeAlso({ObjectFactory.class})
public interface PaymentGatewayImplementationServicePortType {

    @WebMethod(operationName = "RefreshAuthority", action = "#RefreshAuthority")
    @RequestWrapper(localName = "RefreshAuthority", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.RefreshAuthority")
    @ResponseWrapper(localName = "RefreshAuthorityResponse", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.RefreshAuthorityResponse")
    @WebResult(name = "Status", targetNamespace = "http://zarinpal.com/")
    int refreshAuthority(

            @WebParam(name = "MerchantID", targetNamespace = "http://zarinpal.com/")
                    String merchantID,
            @WebParam(name = "Authority", targetNamespace = "http://zarinpal.com/")
                    String authority,
            @WebParam(name = "ExpireIn", targetNamespace = "http://zarinpal.com/")
                    int expireIn
    );

    @WebMethod(operationName = "PaymentRequest", action = "#PaymentRequest")
    @RequestWrapper(localName = "PaymentRequest", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentRequest")
    @ResponseWrapper(localName = "PaymentRequestResponse", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentRequestResponse")
    void paymentRequest(

            @WebParam(name = "MerchantID", targetNamespace = "http://zarinpal.com/")
                    String merchantID,
            @WebParam(name = "Amount", targetNamespace = "http://zarinpal.com/")
                    int amount,
            @WebParam(name = "Description", targetNamespace = "http://zarinpal.com/")
                    String description,
            @WebParam(name = "Email", targetNamespace = "http://zarinpal.com/")
                    String email,
            @WebParam(name = "Mobile", targetNamespace = "http://zarinpal.com/")
                    String mobile,
            @WebParam(name = "CallbackURL", targetNamespace = "http://zarinpal.com/")
                    String callbackURL,
            @WebParam(mode = WebParam.Mode.OUT, name = "Status", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<Integer> status,
            @WebParam(mode = WebParam.Mode.OUT, name = "Authority", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<String> authority
    );

    @WebMethod(operationName = "PaymentRequestWithExtra", action = "#PaymentRequestWithExtra")
    @RequestWrapper(localName = "PaymentRequestWithExtra", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentRequestWithExtra")
    @ResponseWrapper(localName = "PaymentRequestWithExtraResponse", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentRequestWithExtraResponse")
    void paymentRequestWithExtra(

            @WebParam(name = "MerchantID", targetNamespace = "http://zarinpal.com/")
                    String merchantID,
            @WebParam(name = "Amount", targetNamespace = "http://zarinpal.com/")
                    int amount,
            @WebParam(name = "Description", targetNamespace = "http://zarinpal.com/")
                    String description,
            @WebParam(name = "AdditionalData", targetNamespace = "http://zarinpal.com/")
                    String additionalData,
            @WebParam(name = "Email", targetNamespace = "http://zarinpal.com/")
                    String email,
            @WebParam(name = "Mobile", targetNamespace = "http://zarinpal.com/")
                    String mobile,
            @WebParam(name = "CallbackURL", targetNamespace = "http://zarinpal.com/")
                    String callbackURL,
            @WebParam(mode = WebParam.Mode.OUT, name = "Status", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<Integer> status,
            @WebParam(mode = WebParam.Mode.OUT, name = "Authority", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<String> authority
    );

    @WebMethod(operationName = "PaymentVerification", action = "#PaymentVerification")
    @RequestWrapper(localName = "PaymentVerification", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentVerification")
    @ResponseWrapper(localName = "PaymentVerificationResponse", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentVerificationResponse")
    void paymentVerification(

            @WebParam(name = "MerchantID", targetNamespace = "http://zarinpal.com/")
                    String merchantID,
            @WebParam(name = "Authority", targetNamespace = "http://zarinpal.com/")
                    String authority,
            @WebParam(name = "Amount", targetNamespace = "http://zarinpal.com/")
                    int amount,
            @WebParam(mode = WebParam.Mode.OUT, name = "Status", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<Integer> status,
            @WebParam(mode = WebParam.Mode.OUT, name = "RefID", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<Long> refID
    );

    @WebMethod(operationName = "PaymentVerificationWithExtra", action = "#PaymentVerificationWithExtra")
    @RequestWrapper(localName = "PaymentVerificationWithExtra", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentVerificationWithExtra")
    @ResponseWrapper(localName = "PaymentVerificationWithExtraResponse", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.PaymentVerificationWithExtraResponse")
    void paymentVerificationWithExtra(

            @WebParam(name = "MerchantID", targetNamespace = "http://zarinpal.com/")
                    String merchantID,
            @WebParam(name = "Authority", targetNamespace = "http://zarinpal.com/")
                    String authority,
            @WebParam(name = "Amount", targetNamespace = "http://zarinpal.com/")
                    int amount,
            @WebParam(mode = WebParam.Mode.OUT, name = "Status", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<Integer> status,
            @WebParam(mode = WebParam.Mode.OUT, name = "RefID", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<Long> refID,
            @WebParam(mode = WebParam.Mode.OUT, name = "ExtraDetail", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<String> extraDetail
    );

    @WebMethod(operationName = "GetUnverifiedTransactions", action = "#GetUnverifiedTransactions")
    @RequestWrapper(localName = "GetUnverifiedTransactions", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.GetUnverifiedTransactions")
    @ResponseWrapper(localName = "GetUnverifiedTransactionsResponse", targetNamespace = "http://zarinpal.com/", className = "com.zarinpal.GetUnverifiedTransactionsResponse")
    void getUnverifiedTransactions(

            @WebParam(name = "MerchantID", targetNamespace = "http://zarinpal.com/")
                    String merchantID,
            @WebParam(mode = WebParam.Mode.OUT, name = "Status", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<Integer> status,
            @WebParam(mode = WebParam.Mode.OUT, name = "Authorities", targetNamespace = "http://zarinpal.com/")
                    javax.xml.ws.Holder<String> authorities
    );
}
