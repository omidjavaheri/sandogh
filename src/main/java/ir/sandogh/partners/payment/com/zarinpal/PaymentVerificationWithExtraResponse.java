package ir.sandogh.partners.payment.com.zarinpal;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RefID" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="ExtraDetail" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "status",
        "refID",
        "extraDetail"
})
@XmlRootElement(name = "PaymentVerificationWithExtraResponse")
public class PaymentVerificationWithExtraResponse {

    @XmlElement(name = "Status")
    protected int status;
    @XmlElement(name = "RefID")
    protected long refID;
    @XmlElement(name = "ExtraDetail", required = true)
    protected String extraDetail;

    /**
     * Gets the value of the status property.
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Gets the value of the refID property.
     */
    public long getRefID() {
        return refID;
    }

    /**
     * Sets the value of the refID property.
     */
    public void setRefID(long value) {
        this.refID = value;
    }

    /**
     * Gets the value of the extraDetail property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getExtraDetail() {
        return extraDetail;
    }

    /**
     * Sets the value of the extraDetail property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setExtraDetail(String value) {
        this.extraDetail = value;
    }

}
