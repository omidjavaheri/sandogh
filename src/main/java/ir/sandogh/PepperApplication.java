package ir.sandogh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
@PropertySource(value = "classpath:messages.properties", encoding = "UTF-8")
public class PepperApplication {
    public static void main(String[] args) {
        SpringApplication.run(PepperApplication.class, args);
    }
}
