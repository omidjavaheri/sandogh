package ir.sandogh.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.sandogh.dto.v1.UserDTO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "auth_users")
public class User implements UserDetails {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "username", unique = true)
    private String username;
    private String password;
    private String plain;
    private String role;
    private String name;
    private String family;
    private String nationalCode;
    @Column(name = "mobile", unique = true)
    private String mobile;
    private String phone;
    private String email;
    private String address;
    private String field;
    private String employeeNum;
    private String membershipNum;
    private String province;
    private String city;
    // recoveryCode / recoveryCodeCounter / tokenCode / tokenCodeCounter
    @JsonIgnore
    private String recoveryField;
    @JsonIgnore
    private boolean enabled = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlain() {
        return plain;
    }

    public void setPlain(String plain) {
        this.plain = plain;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getRecoveryField() {
        return recoveryField;
    }

    public void setRecoveryField(String recoveryField) {
        this.recoveryField = recoveryField;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(this.role);
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getEmployeeNum() {
        return employeeNum;
    }

    public void setEmployeeNum(String employeeNum) {
        this.employeeNum = employeeNum;
    }

    public String getMembershipNum() {
        return membershipNum;
    }

    public void setMembershipNum(String membershipNum) {
        this.membershipNum = membershipNum;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<String> convertServerRolesToClient() {
        List<String> clients = new ArrayList<>();
        this.getAuthorities().forEach(role -> {
            clients.add(Role.findClientRole(role.getAuthority()));
        });
        return clients;
    }

    public UserDTO convertEntityToDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setName(this.getName());
        userDTO.setFamily(this.getFamily());
        userDTO.setNationalCode(this.getNationalCode());
        userDTO.setMobile(this.getMobile());
        userDTO.setPhone(this.getPhone());
        userDTO.setEmail(this.getEmail());
        userDTO.setAddress(this.getAddress());
        userDTO.setField(this.getField());
        userDTO.setEmployeeNum(this.getEmployeeNum());
        userDTO.setMembershipNum(this.getMembershipNum());
        userDTO.setProvince(this.getProvince());
        userDTO.setCity(this.getCity());
        userDTO.setUsername(this.getUsername());
        userDTO.setRoles(convertServerRolesToClient());
        return userDTO;
    }

    public void populateEntityByDTO(UserDTO dto) {
        this.setName(dto.getName());
        this.setFamily(dto.getFamily());
        this.setNationalCode(dto.getNationalCode());
        this.setMobile(dto.getMobile());
        this.setPhone(dto.getPhone());
        this.setEmail(dto.getEmail());
        this.setAddress(dto.getAddress());
        this.setField(dto.getField());
        this.setEmployeeNum(dto.getEmployeeNum());
        this.setMembershipNum(dto.getMembershipNum());
        this.setProvince(dto.getProvince());
        this.setCity(dto.getCity());
    }
}
