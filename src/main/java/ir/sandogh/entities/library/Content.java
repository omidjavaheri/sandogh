package ir.sandogh.entities.library;

import javax.persistence.*;

@Entity
@Table(name = "library_content")
public class Content {

    @Id
    @GeneratedValue
    private Long id;
    @Enumerated
    private ContentType contentType;
    private String field;
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
