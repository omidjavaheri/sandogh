package ir.sandogh.entities.library;

public enum ContentType {
    DOC, AUDIO, VIDEO;
}
