package ir.sandogh.entities.insurance.policy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;

import javax.persistence.*;

@Entity
@Table(name = "insurance_agency")
public class Agency {
    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Insurer insurer;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
