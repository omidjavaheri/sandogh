package ir.sandogh.entities.insurance.policy;

public enum PolicyState {
    SUBMITTED, PAID, LOCKED, ISSUED
}
