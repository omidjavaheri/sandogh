package ir.sandogh.entities.insurance.policy;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.entities.insurance.inquiry.lookup.InsuranceType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "insurance_policy")
public class Policy {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Insurer insurer;

    @ManyToOne
    private Agency agency;

    @Enumerated
    private InsuranceType type;

    private String assignedAgentUsername;

    private Long price;

    private LocalDateTime submitDate;

    private LocalDateTime issueDate;

    @Enumerated
    private PolicyState state;

    private String mobile;

    private String email;

    private String userInfoJson;

    private String paymentJson;

    private String documentsUrlJson;

    private String inquiryJson;

    private String username;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public InsuranceType getType() {
        return type;
    }

    public void setType(InsuranceType type) {
        this.type = type;
    }

    public String getAssignedAgentUsername() {
        return assignedAgentUsername;
    }

    public void setAssignedAgentUsername(String assignedAgentUsername) {
        this.assignedAgentUsername = assignedAgentUsername;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public LocalDateTime getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(LocalDateTime submitDate) {
        this.submitDate = submitDate;
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public PolicyState getState() {
        return state;
    }

    public void setState(PolicyState state) {
        this.state = state;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserInfoJson() {
        return userInfoJson;
    }

    public void setUserInfoJson(String userInfoJson) {
        this.userInfoJson = userInfoJson;
    }

    public String getPaymentJson() {
        return paymentJson;
    }

    public void setPaymentJson(String paymentJson) {
        this.paymentJson = paymentJson;
    }

    public String getDocumentsUrlJson() {
        return documentsUrlJson;
    }

    public void setDocumentsUrlJson(String documentsUrlJson) {
        this.documentsUrlJson = documentsUrlJson;
    }

    public String getInquiryJson() {
        return inquiryJson;
    }

    public void setInquiryJson(String inquiryJson) {
        this.inquiryJson = inquiryJson;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
