package ir.sandogh.entities.insurance.policy;

import javax.persistence.*;

@Entity
@Table(name = "insurance_agent")
public class Agent {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Agency agency;

    private String username;

    private boolean supervisor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isSupervisor() {
        return supervisor;
    }

    public void setSupervisor(boolean supervisor) {
        this.supervisor = supervisor;
    }
}
