package ir.sandogh.entities.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;

import javax.persistence.*;

@Entity
@Table(name = "body_age_rate")
public class AgeRate {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Insurer insurer;

    private int startingYear;

    private double eachYearRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public int getStartingYear() {
        return startingYear;
    }

    public void setStartingYear(int startingYear) {
        this.startingYear = startingYear;
    }

    public double getEachYearRate() {
        return eachYearRate;
    }

    public void setEachYearRate(double eachYearRate) {
        this.eachYearRate = eachYearRate;
    }
}
