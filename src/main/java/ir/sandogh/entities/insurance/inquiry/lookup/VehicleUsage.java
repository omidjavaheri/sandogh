package ir.sandogh.entities.insurance.inquiry.lookup;

import javax.persistence.*;

@Entity
@Table(name = "lookup_vehicle_usage")
public class VehicleUsage {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private VehicleGroup vehicleGroup;

    @ManyToOne
    private VehicleUsageType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleUsageType getType() {
        return type;
    }

    public void setType(VehicleUsageType type) {
        this.type = type;
    }

    public VehicleGroup getVehicleGroup() {
        return vehicleGroup;
    }

    public void setVehicleGroup(VehicleGroup vehicleGroup) {
        this.vehicleGroup = vehicleGroup;
    }
}
