package ir.sandogh.entities.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleUsage;

import javax.persistence.*;

@Entity
@Table(name = "body_rate")
public class BodyRate {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private VehicleUsage vehicleUsage;

    private double rate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleUsage getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(VehicleUsage vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
