package ir.sandogh.entities.insurance.inquiry.lookup;


import javax.persistence.*;

@Entity
@Table(name = "lookup_vehicle_kind")
public class VehicleKind {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Insurer insurer;

    @ManyToOne
    private VehicleKindType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public VehicleKindType getType() {
        return type;
    }

    public void setType(VehicleKindType type) {
        this.type = type;
    }
}
