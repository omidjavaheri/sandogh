package ir.sandogh.entities.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleKind;

import javax.persistence.*;

@Entity
@Table(name = "third_party_financial_surplus")
public class FinancialSurplus {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private VehicleKind vehicleKind;

    private double coverageAmount;

    private double financialSurplus;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleKind getVehicleKind() {
        return vehicleKind;
    }

    public void setVehicleKind(VehicleKind vehicleKind) {
        this.vehicleKind = vehicleKind;
    }

    public double getCoverageAmount() {
        return coverageAmount;
    }

    public void setCoverageAmount(double coverageAmount) {
        this.coverageAmount = coverageAmount;
    }

    public double getFinancialSurplus() {
        return financialSurplus;
    }

    public void setFinancialSurplus(double financialSurplus) {
        this.financialSurplus = financialSurplus;
    }
}
