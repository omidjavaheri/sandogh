package ir.sandogh.entities.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleUsage;

import javax.persistence.*;

@Entity
@Table(name = "body_additional_cover_rate")
public class AdditionalCoverRate {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private VehicleUsage vehicleUsage;

    @ManyToOne
    private AdditionalCoverType type;

    private double rate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleUsage getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(VehicleUsage vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    public AdditionalCoverType getType() {
        return type;
    }

    public void setType(AdditionalCoverType type) {
        this.type = type;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
