package ir.sandogh.entities.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;

import javax.persistence.*;

@Entity
@Table(name = "third_party_insurer_discount")
public class InsurerDiscount {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Insurer insurer;

    private double discount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
