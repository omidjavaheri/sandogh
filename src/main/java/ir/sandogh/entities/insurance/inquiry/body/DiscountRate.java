package ir.sandogh.entities.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;

import javax.persistence.*;

@Entity
@Table(name = "body_discount_rate")
public class DiscountRate {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Insurer insurer;

    @ManyToOne
    private DiscountType type;

    private double rate;

    public DiscountRate() {
    }

    public DiscountRate(Insurer insurer, DiscountType type, double rate) {
        this.insurer = insurer;
        this.type = type;
        this.rate = rate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public DiscountType getType() {
        return type;
    }

    public void setType(DiscountType type) {
        this.type = type;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
