package ir.sandogh.entities.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleGroup;

import javax.persistence.*;

@Entity
@Table(name = "third_party_driver_accident_rate")
public class DriverAccidentRate {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private VehicleGroup vehicleGroup;

    private double rate;

    public DriverAccidentRate() {
    }

    public DriverAccidentRate(VehicleGroup vehicleGroup, double nerkheHavadesRanande) {
        this.vehicleGroup = vehicleGroup;
        this.rate = nerkheHavadesRanande;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleGroup getVehicleGroup() {
        return vehicleGroup;
    }

    public void setVehicleGroup(VehicleGroup vehicleGroup) {
        this.vehicleGroup = vehicleGroup;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
