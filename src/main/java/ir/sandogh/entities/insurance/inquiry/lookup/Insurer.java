package ir.sandogh.entities.insurance.inquiry.lookup;

import javax.persistence.*;

@Entity
@Table(name = "lookup_insurer")
public class Insurer {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    public Insurer() {
    }

    public Insurer(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

