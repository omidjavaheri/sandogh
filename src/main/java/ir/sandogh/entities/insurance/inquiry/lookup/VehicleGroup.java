package ir.sandogh.entities.insurance.inquiry.lookup;

import javax.persistence.*;

@Entity
@Table(name = "lookup_vehicle_group")
public class VehicleGroup {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Insurer insurer;

    @ManyToOne
    private VehicleGroupType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public VehicleGroupType getType() {
        return type;
    }

    public void setType(VehicleGroupType type) {
        this.type = type;
    }
}
