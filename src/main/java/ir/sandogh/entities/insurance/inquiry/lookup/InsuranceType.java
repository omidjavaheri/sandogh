package ir.sandogh.entities.insurance.inquiry.lookup;


public enum  InsuranceType {
    THIRD_PARTY,BODY,SUPPLEMENTARY,FIRE,LIFE
}
