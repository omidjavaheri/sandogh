package ir.sandogh.entities.insurance.inquiry.supplementary;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;

import javax.persistence.*;

@Entity
@Table(name = "supplementary_basic_insurer_rate")
public class BasicInsurerRate {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Insurer insurer;

    private double increaseRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Insurer getInsurer() {
        return insurer;
    }

    public void setInsurer(Insurer insurer) {
        this.insurer = insurer;
    }

    public double getIncreaseRate() {
        return increaseRate;
    }

    public void setIncreaseRate(double increaseRate) {
        this.increaseRate = increaseRate;
    }
}
