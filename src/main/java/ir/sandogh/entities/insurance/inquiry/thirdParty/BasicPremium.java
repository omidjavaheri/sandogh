package ir.sandogh.entities.insurance.inquiry.thirdParty;


import ir.sandogh.entities.insurance.inquiry.lookup.VehicleKind;

import javax.persistence.*;

@Entity
@Table(name = "third_party_basic_premium")
public class BasicPremium {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private VehicleKind vehicleKind;

    private double basicPremium;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleKind getVehicleKind() {
        return vehicleKind;
    }

    public void setVehicleKind(VehicleKind vehicleKind) {
        this.vehicleKind = vehicleKind;
    }

    public double getBasicPremium() {
        return basicPremium;
    }

    public void setBasicPremium(double basicPremium) {
        this.basicPremium = basicPremium;
    }
}
