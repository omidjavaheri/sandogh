package ir.sandogh.entities;

import org.springframework.security.core.GrantedAuthority;

import java.util.HashMap;
import java.util.Map;

public enum Role {
    //**_RO(XX)LE_**
    ACTUXXATOR_ADMXXIN("ROLE_ACTUXXATOR_ADMXXIN", "Han_Solo"),             //0 actuator admin
    CUSTXXOMER("ROLE_CUSTXXOMER", "Darth_Vader"),                 //1 customer
    ADXXMIN("ROLE_ADXXMIN", "Princess_Leia"),              //2 admin
    AGEXXNT("ROLE_AGEXXNT", "Luke_Skywalker"),              //3 agent
    SUPERXXVISOR("ROLE_SUPERXXVISOR", "Obi-Wan_Kenobi"),         //4 supervisor
    CLXXUB_ADXXMIN("ROLE_CLXXUB_ADXXMIN", "Yoda"),              //5 club admin
    ORGANIZXXATION_ADXXMIN("ROLE_ORGANIZXXATION_ADXXMIN", "Chewbacca"),  //6 organization admin
    SHXXOP_OWXXNER("ROLE_SHXXOP_OWXXNER", "Boba_Fett");              //7 shop owner

    private final String server;
    private final String client;

    private static final Map<String,String> serverMap;
    private static final Map<String,String> clientMap;

    static {
        serverMap = new HashMap<>();
        clientMap = new HashMap<>();
        for (Role v : Role.values()) {
            serverMap.put(v.client, v.server);
            clientMap.put(v.server, v.client);
        }
    }

    Role(String server, String client) {
        this.server = server;
        this.client = client;
    }

    public static String findServerRole(String client) {
        return serverMap.get(client);
    }

    public static String findClientRole(String server) {
        return clientMap.get(server);
    }

    public String getServer() {
        return server;
    }

    public String getClient() {
        return client;
    }

}
