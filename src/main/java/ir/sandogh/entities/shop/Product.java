package ir.sandogh.entities.shop;

import javax.persistence.*;

@Entity
@Table(name = "shop_product")
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    private float rate;

    private double price;

    private String imageUrl;

    private String url;

    private String shopOwnerUsername;

    @ManyToOne
    private Club club;

    @Enumerated
    private ProductState state;

    public String getDescription() {
        return description;
    }

    public String getShopOwnerUsername() {
        return shopOwnerUsername;
    }

    public void setShopOwnerUsername(String shopOwnerUsername) {
        this.shopOwnerUsername = shopOwnerUsername;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public ProductState getState() {
        return state;
    }

    public void setState(ProductState state) {
        this.state = state;
    }
}
