package ir.sandogh.utils;

import ir.sandogh.partners.sms.kavenegar.sdk.KavenegarApi;
import ir.sandogh.partners.sms.kavenegar.sdk.utils.PairValue;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class SmsSenderService {

    private KavenegarApi api;

    @PostConstruct
    public void initIt() throws Exception {
        api = new KavenegarApi("343859416D645052787038353358683172615838676C684E6D6247516C374564313579516E56666E584B673D");
    }

    public void sendSms(String sender,String receptor,String message){
        api.send(sender, receptor, message);
    }

    public void sendTemplateSms(String receptor, String token, String token2, String token3, String template, List<PairValue> params){
        api.verifyLookup(receptor,token,token2,token3,template, params);
    }

}
