package ir.sandogh.repositories.library;

import ir.sandogh.entities.library.Content;
import ir.sandogh.entities.library.ContentType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContentRepository extends PagingAndSortingRepository<Content,Long> {
    @Query("select c from Content c where (:cType is null or c.contentType = :cType) and (:cField is null or c.field = :cField)")
    List<Content> findByContentTypeAndField(@Param("cType") ContentType type, @Param("cField") String field, Pageable pageable);
}
