package ir.sandogh.repositories;

import ir.sandogh.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByUsername(String username);
    User findByMobile(String mobile);
}
