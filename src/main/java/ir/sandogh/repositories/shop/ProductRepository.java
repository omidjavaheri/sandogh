package ir.sandogh.repositories.shop;

import ir.sandogh.entities.shop.Product;
import ir.sandogh.entities.shop.ProductState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Iterable<Product> findAllByStateOrderByIdDesc(ProductState state);
    Iterable<Product> findAllByShopOwnerUsernameOrderByIdDesc(String username);
}
