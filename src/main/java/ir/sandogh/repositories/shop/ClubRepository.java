package ir.sandogh.repositories.shop;

import ir.sandogh.entities.shop.Club;
import org.springframework.data.repository.CrudRepository;

public interface ClubRepository extends CrudRepository<Club, Long> {
}
