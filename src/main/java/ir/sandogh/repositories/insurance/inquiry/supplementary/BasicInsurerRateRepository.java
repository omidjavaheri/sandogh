package ir.sandogh.repositories.insurance.inquiry.supplementary;

import ir.sandogh.entities.insurance.inquiry.supplementary.BasicInsurerRate;
import org.springframework.data.repository.CrudRepository;

public interface BasicInsurerRateRepository extends CrudRepository<BasicInsurerRate, Long> {
}
