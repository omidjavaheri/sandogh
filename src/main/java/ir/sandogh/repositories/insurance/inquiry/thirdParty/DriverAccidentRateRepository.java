package ir.sandogh.repositories.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.DriverAccidentRate;
import org.springframework.data.repository.CrudRepository;

public interface DriverAccidentRateRepository extends CrudRepository<DriverAccidentRate,Long> {
}
