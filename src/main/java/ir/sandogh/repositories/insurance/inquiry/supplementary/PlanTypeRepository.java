package ir.sandogh.repositories.insurance.inquiry.supplementary;

import ir.sandogh.entities.insurance.inquiry.supplementary.PlanType;
import org.springframework.data.repository.CrudRepository;

public interface PlanTypeRepository extends CrudRepository<PlanType,Long> {
}
