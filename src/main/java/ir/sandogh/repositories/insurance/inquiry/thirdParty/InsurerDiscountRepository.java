package ir.sandogh.repositories.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.InsurerDiscount;
import org.springframework.data.repository.CrudRepository;

public interface InsurerDiscountRepository extends CrudRepository<InsurerDiscount,Long> {
}
