package ir.sandogh.repositories.insurance.inquiry.thirdParty;


import ir.sandogh.entities.insurance.inquiry.thirdParty.BasicPremium;
import org.springframework.data.repository.CrudRepository;

public interface BasicPremiumRepository extends CrudRepository<BasicPremium,Long> {
}
