package ir.sandogh.repositories.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.AdditionalCoverType;
import org.springframework.data.repository.CrudRepository;

public interface AdditionalCoverTypeRepository extends CrudRepository<AdditionalCoverType,Long> {
}
