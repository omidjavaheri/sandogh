package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import org.springframework.data.repository.CrudRepository;

public interface InsurerRepository extends CrudRepository<Insurer,Long> {
}
