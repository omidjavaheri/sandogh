package ir.sandogh.repositories.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.UsageRate;
import org.springframework.data.repository.CrudRepository;

public interface UsageRateRepository extends CrudRepository<UsageRate,Long> {
}
