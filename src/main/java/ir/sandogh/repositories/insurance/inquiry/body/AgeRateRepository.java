package ir.sandogh.repositories.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.AgeRate;
import org.springframework.data.repository.CrudRepository;

public interface AgeRateRepository extends CrudRepository<AgeRate,Long> {
}
