package ir.sandogh.repositories.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.DiscountType;
import org.springframework.data.repository.CrudRepository;

public interface DiscountTypeRepository extends CrudRepository<DiscountType,Long> {
}
