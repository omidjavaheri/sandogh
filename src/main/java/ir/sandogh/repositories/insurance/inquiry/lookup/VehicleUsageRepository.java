package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleUsage;
import org.springframework.data.repository.CrudRepository;

public interface VehicleUsageRepository extends CrudRepository<VehicleUsage,Long> {
}
