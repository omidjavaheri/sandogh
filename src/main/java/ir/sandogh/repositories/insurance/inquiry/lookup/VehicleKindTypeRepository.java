package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleKindType;
import org.springframework.data.repository.CrudRepository;

public interface VehicleKindTypeRepository extends CrudRepository<VehicleKindType,Long> {
}
