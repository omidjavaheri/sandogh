package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.BloodMoney;
import org.springframework.data.repository.CrudRepository;

public interface BloodMoneyRepository extends CrudRepository<BloodMoney,Long> {
}
