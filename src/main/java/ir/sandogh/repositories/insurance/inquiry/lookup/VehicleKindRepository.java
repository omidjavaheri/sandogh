package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleKind;
import org.springframework.data.repository.CrudRepository;

public interface VehicleKindRepository extends CrudRepository<VehicleKind,Long> {
}
