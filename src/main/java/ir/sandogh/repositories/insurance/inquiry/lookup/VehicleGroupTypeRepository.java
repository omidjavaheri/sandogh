package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleGroupType;
import org.springframework.data.repository.CrudRepository;

public interface VehicleGroupTypeRepository extends CrudRepository<VehicleGroupType,Long> {
}
