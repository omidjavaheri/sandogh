package ir.sandogh.repositories.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.FinancialSurplus;
import org.springframework.data.repository.CrudRepository;

public interface FinancialSurplusRepository extends CrudRepository<FinancialSurplus,Long> {
}
