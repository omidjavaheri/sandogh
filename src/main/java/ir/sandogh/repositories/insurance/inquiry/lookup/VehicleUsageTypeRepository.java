package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleUsageType;
import org.springframework.data.repository.CrudRepository;

public interface VehicleUsageTypeRepository extends CrudRepository<VehicleUsageType,Long> {
}
