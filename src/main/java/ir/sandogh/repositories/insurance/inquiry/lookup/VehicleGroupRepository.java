package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleGroup;
import org.springframework.data.repository.CrudRepository;

public interface VehicleGroupRepository extends CrudRepository<VehicleGroup,Long> {
}
