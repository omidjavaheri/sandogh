package ir.sandogh.repositories.insurance.inquiry.supplementary;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.entities.insurance.inquiry.supplementary.SupplementaryPremium;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SupplementaryPremiumRepository extends CrudRepository<SupplementaryPremium,Long> {
    Iterable<SupplementaryPremium> findByInsurerAndAgeGroup(Insurer insurer, int ageGroup);

    @Query("SELECT DISTINCT a.ageGroup FROM SupplementaryPremium a")
    Iterable<Integer> findDistinctAgeGroup();
}
