package ir.sandogh.repositories.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.TaxRate;
import org.springframework.data.repository.CrudRepository;

public interface TaxRateRepository extends CrudRepository<TaxRate,Long> {
}
