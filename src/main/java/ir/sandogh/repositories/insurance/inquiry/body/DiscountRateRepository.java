package ir.sandogh.repositories.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.DiscountRate;
import org.springframework.data.repository.CrudRepository;

public interface DiscountRateRepository extends CrudRepository<DiscountRate,Long> {
}
