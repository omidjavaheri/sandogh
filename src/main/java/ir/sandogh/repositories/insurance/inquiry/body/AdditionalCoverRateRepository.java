package ir.sandogh.repositories.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.AdditionalCoverRate;
import org.springframework.data.repository.CrudRepository;

public interface AdditionalCoverRateRepository extends CrudRepository<AdditionalCoverRate,Long> {
}
