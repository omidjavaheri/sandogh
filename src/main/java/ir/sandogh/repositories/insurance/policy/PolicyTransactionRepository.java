package ir.sandogh.repositories.insurance.policy;

import ir.sandogh.entities.insurance.policy.PolicyTransaction;
import org.springframework.data.repository.CrudRepository;

public interface PolicyTransactionRepository extends CrudRepository<PolicyTransaction,Long> {

    PolicyTransaction findByAuthority(String authority);
}
