package ir.sandogh.repositories.insurance.policy;

import ir.sandogh.entities.insurance.policy.Agency;
import org.springframework.data.repository.CrudRepository;

public interface AgencyRepository extends CrudRepository<Agency,Long> {
}
