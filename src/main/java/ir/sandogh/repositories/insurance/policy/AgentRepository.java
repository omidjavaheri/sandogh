package ir.sandogh.repositories.insurance.policy;

import ir.sandogh.entities.insurance.policy.Agent;
import org.springframework.data.repository.CrudRepository;

public interface AgentRepository extends CrudRepository<Agent,Long> {
    Agent findByUsername(String username);
}
