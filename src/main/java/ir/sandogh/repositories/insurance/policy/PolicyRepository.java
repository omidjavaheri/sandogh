package ir.sandogh.repositories.insurance.policy;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.entities.insurance.policy.Agency;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.entities.insurance.policy.PolicyState;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PolicyRepository extends PagingAndSortingRepository<Policy, Long> {
    Long countByAgency(Agency agency);

    Long countByAssignedAgentUsername(String agentUsername);

    List<Policy> findAllByStateAndAssignedAgentUsername(PolicyState state, String username, Pageable pageable);

    List<Policy> findAllByStateAndInsurer(PolicyState state, Insurer insurer, Pageable pageable);
}
