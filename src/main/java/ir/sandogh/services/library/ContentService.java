package ir.sandogh.services.library;

import ir.sandogh.dto.v1.library.ContentDTO;
import ir.sandogh.entities.library.Content;
import ir.sandogh.repositories.library.ContentRepository;
import ir.sandogh.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ContentService extends BaseService {

    @Autowired
    private ContentRepository contentRepository;

    public void save(Content content) {
        contentRepository.save(content);
    }

    public void delete(Long id) {
        try{
            contentRepository.deleteById(id);
        }catch (Exception e){
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }

    }

    public List<Content> findByTypeAndField(ContentDTO dto) {
        return contentRepository.findByContentTypeAndField(dto.getContent().getContentType(), dto.getContent().getField(), getPageable(dto.getPageableDTO()));
    }
}
