package ir.sandogh.services.shop;

import ir.sandogh.dto.v1.shop.UpdateStateDTO;
import ir.sandogh.entities.shop.Product;
import ir.sandogh.entities.shop.ProductState;
import ir.sandogh.repositories.shop.ProductRepository;
import ir.sandogh.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class ShopService extends BaseService {
    @Autowired
    private ProductRepository productRepository;

    public Iterable<Product> findAll(ProductState state) {
        return productRepository.findAllByStateOrderByIdDesc(state);
    }

    public void deleteById(Long id) {
        try {
            productRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("productNotFound"));
        }

    }

    public Iterable<Product> findAllByShopOwnerUsername() {
        return productRepository.findAllByShopOwnerUsernameOrderByIdDesc(getLoggedInUsername());
    }

    public Product save(Product product) {
        product.setShopOwnerUsername(getLoggedInUsername());
        return productRepository.save(product);
    }

    public Product updateSate(UpdateStateDTO dto) {
        Optional<Product> product = productRepository.findById(dto.getId());
        if (product.isPresent()) {
            product.get().setState(dto.getState());
        }
        return product
                .orElseThrow(() -> new RuntimeException(getErrorMessage("productNotFound")));
    }
}
