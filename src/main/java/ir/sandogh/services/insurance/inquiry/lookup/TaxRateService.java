package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.TaxRate;
import ir.sandogh.repositories.insurance.inquiry.lookup.TaxRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaxRateService {
    @Autowired
    private TaxRateRepository taxRateRepository;

    public Iterable<TaxRate> findAll() {
        return taxRateRepository.findAll();
    }
}
