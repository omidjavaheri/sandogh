package ir.sandogh.services.insurance.inquiry.thirdParty;


import ir.sandogh.entities.insurance.inquiry.thirdParty.UsageRate;
import ir.sandogh.repositories.insurance.inquiry.thirdParty.UsageRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsageRateService {
    @Autowired
    private UsageRateRepository usageRateRepository;

    public Iterable<UsageRate> findAll() {
        return usageRateRepository.findAll();
    }

}
