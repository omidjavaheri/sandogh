package ir.sandogh.services.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.InsurerDiscount;
import ir.sandogh.repositories.insurance.inquiry.thirdParty.InsurerDiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InsurerDiscountService {
    @Autowired
    private InsurerDiscountRepository insurerDiscountRepository;

    public Iterable<InsurerDiscount> findAll() {
        return insurerDiscountRepository.findAll();
    }
}
