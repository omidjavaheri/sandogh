package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleUsageType;
import ir.sandogh.repositories.insurance.inquiry.lookup.VehicleUsageTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleUsageTypeService {
    @Autowired
    private VehicleUsageTypeRepository vehicleUsageTypeRepository;

    public Iterable<VehicleUsageType> findAll() {
        return vehicleUsageTypeRepository.findAll();
    }
}
