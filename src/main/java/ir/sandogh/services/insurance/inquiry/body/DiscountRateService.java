package ir.sandogh.services.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.DiscountRate;
import ir.sandogh.repositories.insurance.inquiry.body.DiscountRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscountRateService {
    @Autowired
    private DiscountRateRepository discountRateRepository;

    public Iterable<DiscountRate> findAll() {
        return discountRateRepository.findAll();
    }

}
