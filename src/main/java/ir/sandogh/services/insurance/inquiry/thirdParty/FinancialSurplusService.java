package ir.sandogh.services.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.FinancialSurplus;
import ir.sandogh.repositories.insurance.inquiry.thirdParty.FinancialSurplusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FinancialSurplusService {
    @Autowired
    private FinancialSurplusRepository financialSurplusRepository;

    public Iterable<FinancialSurplus> findAll() {
        return financialSurplusRepository.findAll();
    }

}
