package ir.sandogh.services.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.AgeRate;
import ir.sandogh.repositories.insurance.inquiry.body.AgeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgeRateService {
    @Autowired
    private AgeRateRepository ageRateRepository;

    public Iterable<AgeRate> findAll() {
        return ageRateRepository.findAll();
    }
}
