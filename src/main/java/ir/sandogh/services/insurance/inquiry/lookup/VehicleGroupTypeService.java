package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleGroupType;
import ir.sandogh.repositories.insurance.inquiry.lookup.VehicleGroupTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleGroupTypeService {
    @Autowired
    private VehicleGroupTypeRepository vehicleGroupTypeRepository;

    public Iterable<VehicleGroupType> findAll() {
        return vehicleGroupTypeRepository.findAll();
    }
}
