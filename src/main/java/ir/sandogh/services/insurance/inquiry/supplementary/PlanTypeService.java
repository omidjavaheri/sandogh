package ir.sandogh.services.insurance.inquiry.supplementary;

import ir.sandogh.entities.insurance.inquiry.supplementary.PlanType;
import ir.sandogh.repositories.insurance.inquiry.supplementary.PlanTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanTypeService {
    @Autowired
    private PlanTypeRepository planTypeRepository;

    public Iterable<PlanType> findAll() {
        return planTypeRepository.findAll();
    }
}
