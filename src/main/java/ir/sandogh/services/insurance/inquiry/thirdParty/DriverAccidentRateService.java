package ir.sandogh.services.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.DriverAccidentRate;
import ir.sandogh.repositories.insurance.inquiry.thirdParty.DriverAccidentRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DriverAccidentRateService {
    @Autowired
    private DriverAccidentRateRepository driverAccidentRateRepository;

    public Iterable<DriverAccidentRate> findAll() {
        return driverAccidentRateRepository.findAll();
    }

}
