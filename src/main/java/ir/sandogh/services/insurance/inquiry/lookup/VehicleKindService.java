package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleKind;
import ir.sandogh.repositories.insurance.inquiry.lookup.VehicleKindRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleKindService {
    @Autowired
    private VehicleKindRepository vehicleKindRepository;

    public Iterable<VehicleKind> findAll() {
        return vehicleKindRepository.findAll();
    }
}
