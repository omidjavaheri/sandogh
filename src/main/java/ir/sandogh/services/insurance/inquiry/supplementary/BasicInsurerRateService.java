package ir.sandogh.services.insurance.inquiry.supplementary;

import ir.sandogh.entities.insurance.inquiry.supplementary.BasicInsurerRate;
import ir.sandogh.repositories.insurance.inquiry.supplementary.BasicInsurerRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicInsurerRateService {
    @Autowired
    private BasicInsurerRateRepository basicInsurerRateRepository;

    public Iterable<BasicInsurerRate> findAll() {
        return basicInsurerRateRepository.findAll();
    }
}
