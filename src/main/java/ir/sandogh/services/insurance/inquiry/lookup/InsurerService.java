package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.repositories.insurance.inquiry.lookup.InsurerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InsurerService {
    @Autowired
    private InsurerRepository insurerRepository;

    public Iterable<Insurer> findAll() {
        return insurerRepository.findAll();
    }
}
