package ir.sandogh.services.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.AdditionalCoverType;
import ir.sandogh.repositories.insurance.inquiry.body.AdditionalCoverTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdditionalCoverTypeTypeService {
    @Autowired
    private AdditionalCoverTypeRepository additionalCoverTypeRepository;

    public Iterable<AdditionalCoverType> findAll() {
        return additionalCoverTypeRepository.findAll();
    }
}
