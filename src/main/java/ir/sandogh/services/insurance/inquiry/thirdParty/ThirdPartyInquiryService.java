package ir.sandogh.services.insurance.inquiry.thirdParty;

import ir.sandogh.dto.v1.insurance.inquiry.ResponseDTO;
import ir.sandogh.dto.v1.insurance.inquiry.ThirdPartyDTO;
import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ThirdPartyInquiryService {

    @Autowired
    private double bloodMoney;

    @Resource(name = "insurer_vehicleKind_basicPremium")
    private Map<String, Double> insurer_vehicleKind_basicPremium;

    @Resource(name = "insurer_vehicleGroup_driveAccidentRate")
    private Map<String, Double> insurer_vehicleGroup_driveAccidentRate;

    @Resource(name = "insurer_usageType_increaseRate")
    private Map<String, Double> insurer_usageType_increaseRate;

    @Resource(name = "insurer_discount")
    private Map<Long, Double> insurer_discount;

    @Resource(name = "insurer_vehicleKind_coverageAmount_financialSurplus")
    private Map<String, Double> insurer_vehicleKind_coverageAmount_financialSurplus;

    @Resource(name = "allInsurer")
    private Iterable<Insurer> allInsurer;


    public List<ResponseDTO> inquiry(ThirdPartyDTO dto) {

        List<ResponseDTO> result = new ArrayList<>();

        /**
         *
         *
         *      KhesaratMali:
         *                      1 -> 20%
         *                      2 -> 30%
         *                      3 -> 40%
         *                      ...
         *
         *      KhesaratJani:
         *                      1 -> 30%
         *                      2 -> 70%
         *                      3 -> 100%
         *                      4 -> Reject
         *
         *      KhesaratSarneshin:
         *                      1 -> 30%
         *                      2 -> 70%
         *                      3 -> 100%
         *                      4 -> Reject
         *
         *      TakhfifAdamKhesarat:
         *                      each year -> 5
         *
         *
         */

        if (dto.getPhysicalDamage() > 3 || dto.getPassengerDamage() > 3)
            return result;
        if (dto.getPhysicalDamage() == 1)
            dto.setPhysicalDamage(30);
        else if (dto.getPhysicalDamage() == 2)
            dto.setPhysicalDamage(70);
        else if (dto.getPhysicalDamage() == 3)
            dto.setPhysicalDamage(100);
        if (dto.getFinancialDamage() != 0)
            dto.setFinancialDamage(dto.getFinancialDamage() * 10 + 10);
        if (dto.getPhysicalDamage() > 0)
            dto.setFinancialDamage(0);
        if (dto.getPassengerDamage() == 1)
            dto.setPassengerDamage(30);
        else if (dto.getPassengerDamage() == 2)
            dto.setPassengerDamage(70);
        else if (dto.getPassengerDamage() == 3)
            dto.setPassengerDamage(100);
        for (Insurer insurer : allInsurer) {
            Long BimeID = insurer.getId();

            /**
             *
             *
             * HaghBime = (  [( HaghBimePaye+(HaghBimePaye*RozDirkard)/365 )*( TakhfifAdamKhesarat + KhesaratMali + KhesaratJani )]
             *              +[( NerkhDieh*NerkhHavadesRanande )*( TakhfifeAdamKhesaratSarneshin + KhesaratSarneshin )]
             *              +[ MablaghMazadMali ]  )
             *            *( DarsadMaliat )
             *            *( DarsadAfzaieshKarbari )
             *            *( DarsadTakhfifBimeGar )
             *
             *
             */

            double haghBimePaye = insurer_vehicleKind_basicPremium.get(BimeID + "-" + dto.getVehicleKind());
            double haghBime = (haghBimePaye + (dto.getLatencyInDays() / 365) * haghBimePaye) * (-dto.getNonDamageDiscount() + dto.getPhysicalDamage() + dto.getFinancialDamage() + 100d) / 100d;
            double nerkhSarneshin = 10d * bloodMoney * insurer_vehicleGroup_driveAccidentRate.get(BimeID + "-" + dto.getVehicleGroup()) * ((double) (-dto.getPassengerNonDamageDiscount() + dto.getPassengerDamage()) + 100d) / 100d;
            nerkhSarneshin = round(nerkhSarneshin, 0);
            double temp = haghBime + nerkhSarneshin;
            try {
                double mablaghMazad = insurer_vehicleKind_coverageAmount_financialSurplus.get(BimeID + "-" + dto.getVehicleKind() + "-" + dto.getCoverageAmount()) * (-dto.getNonDamageDiscount() + dto.getPhysicalDamage() + dto.getFinancialDamage() + 100d) / 100d;
                double Result = (temp + mablaghMazad);
                double Result_Karbari = Result * (insurer_usageType_increaseRate.get(BimeID + "-" + dto.getVehicleKind()) + 100d) / 100d;
                double Result_Karbari_takhfif = Result_Karbari * (100d - insurer_discount.get(BimeID)) / 100d;
                ResponseDTO tempDto = new ResponseDTO();
                tempDto.setId(insurer.getId());
                tempDto.setValue(round(Result_Karbari_takhfif,0));
                result.add(tempDto);
            } catch (Exception e) {
            }
        }
        return result;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
