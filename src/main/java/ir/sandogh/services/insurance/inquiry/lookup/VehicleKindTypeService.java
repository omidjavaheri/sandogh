package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleKindType;
import ir.sandogh.repositories.insurance.inquiry.lookup.VehicleKindTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleKindTypeService {
    @Autowired
    private VehicleKindTypeRepository vehicleKindTypeRepository;

    public Iterable<VehicleKindType> findAll() {
        return vehicleKindTypeRepository.findAll();
    }
}
