package ir.sandogh.services.insurance.inquiry.thirdParty;

import ir.sandogh.entities.insurance.inquiry.thirdParty.BasicPremium;
import ir.sandogh.repositories.insurance.inquiry.thirdParty.BasicPremiumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicPremiumService {
    @Autowired
    private BasicPremiumRepository basicPremiumRepository;

    public Iterable<BasicPremium> findAll() {
        return basicPremiumRepository.findAll();
    }

}
