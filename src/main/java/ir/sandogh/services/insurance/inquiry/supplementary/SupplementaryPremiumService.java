package ir.sandogh.services.insurance.inquiry.supplementary;

import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.entities.insurance.inquiry.supplementary.SupplementaryPremium;
import ir.sandogh.repositories.insurance.inquiry.supplementary.SupplementaryPremiumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplementaryPremiumService {
    @Autowired
    private SupplementaryPremiumRepository supplementaryPremiumRepository;

    public Iterable<SupplementaryPremium> findAll() {
        return supplementaryPremiumRepository.findAll();
    }

    public Iterable<SupplementaryPremium> findByBimegarAndAgeGroup(Insurer insurer, int ageGroup) {
        return supplementaryPremiumRepository.findByInsurerAndAgeGroup(insurer, ageGroup);
    }

    public Iterable<Integer> findDistinctAgeGroup() {
        return supplementaryPremiumRepository.findDistinctAgeGroup();
    }
}
