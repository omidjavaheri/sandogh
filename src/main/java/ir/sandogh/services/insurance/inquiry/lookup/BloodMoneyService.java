package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.BloodMoney;
import ir.sandogh.repositories.insurance.inquiry.lookup.BloodMoneyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BloodMoneyService {
    @Autowired
    private BloodMoneyRepository bloodMoneyRepository;

    public Iterable<BloodMoney> findAll() {
        return bloodMoneyRepository.findAll();
    }
}
