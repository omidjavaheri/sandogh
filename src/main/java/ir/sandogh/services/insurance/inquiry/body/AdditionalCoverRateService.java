package ir.sandogh.services.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.AdditionalCoverRate;
import ir.sandogh.repositories.insurance.inquiry.body.AdditionalCoverRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdditionalCoverRateService {
    @Autowired
    private AdditionalCoverRateRepository additionalCoverRateRepository;

    public Iterable<AdditionalCoverRate> findAll() {
        return additionalCoverRateRepository.findAll();
    }

}
