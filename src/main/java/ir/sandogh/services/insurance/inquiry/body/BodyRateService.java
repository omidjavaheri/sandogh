package ir.sandogh.services.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.BodyRate;
import ir.sandogh.repositories.insurance.inquiry.body.BodyRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BodyRateService {
    @Autowired
    private BodyRateRepository bodyRateRepository;

    public Iterable<BodyRate> findAll() {
        return bodyRateRepository.findAll();
    }

}
