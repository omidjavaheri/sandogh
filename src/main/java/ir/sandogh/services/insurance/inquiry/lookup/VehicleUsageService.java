package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleUsage;
import ir.sandogh.repositories.insurance.inquiry.lookup.VehicleUsageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleUsageService {
    @Autowired
    private VehicleUsageRepository VehicleUsageRepository;

    public Iterable<VehicleUsage> findAll() {
        return VehicleUsageRepository.findAll();
    }

}
