package ir.sandogh.services.insurance.inquiry.lookup;

import ir.sandogh.entities.insurance.inquiry.lookup.VehicleGroup;
import ir.sandogh.repositories.insurance.inquiry.lookup.VehicleGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleGroupService {
    @Autowired
    private VehicleGroupRepository vehicleGroupRepository;

    public Iterable<VehicleGroup> findAll() {
        return vehicleGroupRepository.findAll();
    }

}
