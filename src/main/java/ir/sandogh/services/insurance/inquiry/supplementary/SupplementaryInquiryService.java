package ir.sandogh.services.insurance.inquiry.supplementary;

import ir.sandogh.dto.v1.insurance.inquiry.SupplementaryDTO;
import ir.sandogh.dto.v1.insurance.inquiry.SupplementaryResponseDTO;
import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import ir.sandogh.entities.insurance.inquiry.supplementary.SupplementaryPremium;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SupplementaryInquiryService {
    @Resource(name = "insurer_ageGroup_plan_cost")
    private Map<String, Iterable<SupplementaryPremium>> insurer_ageGroup_plan_cost;

    @Resource(name = "insurer_basicInsurerRate")
    private Map<Long, Double> insurer_basicInsurerRate;

    @Resource(name = "allInsurer")
    private Iterable<Insurer> allInsurer;

    public List<SupplementaryResponseDTO> inquiry(SupplementaryDTO dto) {
        List<SupplementaryResponseDTO> result = new ArrayList<>();
        for (Insurer insurer : allInsurer) {
            Double nerkhPosheshBimegarPaye = 0d;
            Double baseRate = insurer_basicInsurerRate.get(insurer.getId());
            if (!dto.isBasicInsurer()) {
                nerkhPosheshBimegarPaye = baseRate != null ? baseRate : 0;
            }
            Iterable<SupplementaryPremium> takmilis = insurer_ageGroup_plan_cost.get(insurer.getId() + "-" + dto.getAgeGroup());
            for (SupplementaryPremium a : takmilis) {
                SupplementaryResponseDTO tempDto = new SupplementaryResponseDTO();
                tempDto.setInsurerId(insurer.getId());
                tempDto.setPlan(a.getPlan());
                tempDto.setCost(a.getCost() * (nerkhPosheshBimegarPaye + 100d) / 100d);
                result.add(tempDto);
            }
        }
        return result;
    }
}
