package ir.sandogh.services.insurance.inquiry.body;

import ir.sandogh.dto.v1.insurance.inquiry.BodyDTO;
import ir.sandogh.dto.v1.insurance.inquiry.ResponseDTO;
import ir.sandogh.entities.insurance.inquiry.lookup.Insurer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class BodyInquiryService {

    @Resource(name = "insurer_ageEachYearRate")
    private Map<Long, Double> insurer_ageEachYearRate;

    @Resource(name = "insurer_ageStartingYear")
    private Map<Long, Integer> insurer_ageStartingYear;

    @Resource(name = "insurer_discountType_discountRate")
    private Map<String, Double> insurer_discountType_discountRate;

    @Resource(name = "insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate")
    private Map<String, Double> insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate;

    @Resource(name = "insurer_vehicleGroup_vehicleUsage_bodyRate")
    private Map<String, Double> insurer_vehicleGroup_vehicleUsage_bodyRate;

    @Resource(name = "allInsurer")
    private Iterable<Insurer> allInsurer;

    public List<ResponseDTO> inquiry(BodyDTO dto) {

        List<ResponseDTO> result = new ArrayList<>();

        /**
         *
         *
         *            -Be ezaie har poshesh ezafi darkhasti
         *            -Poshesh Ezafi Ayab va zohab yek mablagh sabet be rial ast be ba hagh bime nahaii jaam mishavad
         *
         *   PosheshHaieEzafi = shekastShishe + serghatLavazem + balaiaTabiE + NavasanGheimat )
         *
         *
         *            -Be ezaie har takhfif darkhasti -> Zarb Pelekani
         *
         *   Takhfifat = tavafoghi * poshesh * naghdi
         *
         *
         *   HaghBimeAvalie = [( arzeshKhodro * NerkhBimeBadane * PosheshHaieEzafi ) + posheshEzafi_ayabZohab ]
         *                     *( ZaribKohnegiKhodro )
         *                     *( Maliat )
         *                     *( Takhfifat )
         *
         *
         *
         */

        for (Insurer insurer : allInsurer) {
            try {
                double PosheshEzafi = 0d;
                double AyabZohab = 0d;
                if (dto.isAdditionalCover_breakingGlass())
                    PosheshEzafi += insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate.get(insurer.getId() + "-" + dto.getVehicleGroup() + "-" + dto.getVehicleUsage() + "-" + 1);
                if (dto.isAdditionalCover_theft())
                    PosheshEzafi += insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate.get(insurer.getId() + "-" + dto.getVehicleGroup() + "-" + dto.getVehicleUsage() + "-" + 2);
                if (dto.isAdditionalCover_naturalDisaster())
                    PosheshEzafi += insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate.get(insurer.getId() + "-" + dto.getVehicleGroup() + "-" + dto.getVehicleUsage() + "-" + 3);
                if (dto.isAdditionalCover_transportationFee())
                    AyabZohab = insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate.get(insurer.getId() + "-" + dto.getVehicleGroup() + "-" + dto.getVehicleUsage() + "-" + 4);
                if (dto.isAdditionalCover_priceFluctuation())
                    PosheshEzafi += insurer_vehicleGroup_vehicleUsage_additionalCoverRate_rate.get(insurer.getId() + "-" + dto.getVehicleGroup() + "-" + dto.getVehicleUsage() + "-" + 5);
                double ZaribKohnegi;
                int yearDiff = dto.getAge() - insurer_ageStartingYear.get(insurer.getId());
                if (yearDiff > 0)
                    ZaribKohnegi = ((double) yearDiff) * insurer_ageEachYearRate.get(insurer.getId());
                else
                    ZaribKohnegi = 0d;
                double Takhfifat = 1d;
                if (dto.isDiscount_agreedOn())
                    Takhfifat *= (100d - insurer_discountType_discountRate.get(insurer.getId() + "-" + 1)) / 100d;
                if (dto.isDiscount_coverage())
                    Takhfifat *= (100d - insurer_discountType_discountRate.get(insurer.getId() + "-" + 2)) / 100d;
                if (dto.isDiscount_cash())
                    Takhfifat *= (100d - insurer_discountType_discountRate.get(insurer.getId() + "-" + 3)) / 100d;
                double NerkhBimeBadane = insurer_vehicleGroup_vehicleUsage_bodyRate.get(insurer.getId() + "-" + dto.getVehicleGroup() + "-" + dto.getVehicleUsage());
                double Result = dto.getVehicleValue() * (NerkhBimeBadane) / 100d;
                double Result_PosheshEzafi = (Result * (PosheshEzafi + 100d) / 100d) + AyabZohab;
                double Result_PosheshEzafi_Takhfif = Result_PosheshEzafi * (Takhfifat);
                double Result_PosheshEzafi_Takhfif_kohnegi = Result_PosheshEzafi_Takhfif * (ZaribKohnegi + 100d) / 100d;
                ResponseDTO tempDto = new ResponseDTO();
                tempDto.setId(insurer.getId());
                tempDto.setValue(round(Result_PosheshEzafi_Takhfif_kohnegi, 0));
                result.add(tempDto);
            } catch (Exception e) {
            }
        }
        return result;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
