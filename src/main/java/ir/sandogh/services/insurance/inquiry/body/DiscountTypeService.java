package ir.sandogh.services.insurance.inquiry.body;

import ir.sandogh.entities.insurance.inquiry.body.DiscountType;
import ir.sandogh.repositories.insurance.inquiry.body.DiscountTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscountTypeService {
    @Autowired
    private DiscountTypeRepository discountTypeRepository;

    public Iterable<DiscountType> findAll() {
        return discountTypeRepository.findAll();
    }
}
