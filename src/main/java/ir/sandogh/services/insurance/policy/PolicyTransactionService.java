package ir.sandogh.services.insurance.policy;

import ir.sandogh.dto.v1.insurance.policy.AgencyCounterDTO;
import ir.sandogh.dto.v1.insurance.policy.AgentCounterDTO;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.entities.insurance.policy.PolicyState;
import ir.sandogh.entities.insurance.policy.PolicyTransaction;
import ir.sandogh.partners.payment.com.zarinpal.PaymentGatewayImplementationServicePortType;
import ir.sandogh.repositories.insurance.policy.PolicyTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.xml.ws.Holder;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@Service
@Transactional
public class PolicyTransactionService {
    @Resource(name = "insurer_agencyPOJOList")
    private Map<Long, List<AgencyCounterDTO>> insurer_agencyPOJOList;

    @Resource(name = "agency_agentPOJOList")
    private Map<Long, List<AgentCounterDTO>> agency_agentPOJOList;

    @Autowired
    private PolicyTransactionRepository policyTransactionRepository;

    public void save(PolicyTransaction policyTransaction) {
        policyTransactionRepository.save(policyTransaction);
    }

    public PolicyTransaction findByAuthority(String authority) {
        return policyTransactionRepository.findByAuthority(authority);
    }

    public Long verifyAfterPayment(PaymentGatewayImplementationServicePortType service, String merchantId, String authority) {
        PolicyTransaction policyTransaction = findByAuthority(authority);
        Holder<Integer> status = new Holder<>();
        Holder<Long> refID = new Holder<>();
        service.paymentVerification(merchantId, policyTransaction.getAuthority(), policyTransaction.getPrice(), status, refID);
        if (status.value == 100) {
            policyTransaction.setPolicy(assignAgent(policyTransaction.getPolicy()));
            policyTransaction.getPolicy().setState(PolicyState.PAID);
            policyTransaction.setRefId(refID.value);
            policyTransaction.setDate(LocalDateTime.now());
            return refID.value;
        } else {
            return null;
        }

    }

    public Policy assignAgent(Policy policy) {
        List<AgencyCounterDTO> agencies = insurer_agencyPOJOList.get(policy.getInsurer().getId());
        AgencyCounterDTO targetAgency = agencies.stream().min(Comparator.comparing(AgencyCounterDTO::getPolicyCount))
                .orElseThrow(NoSuchElementException::new);
        targetAgency.plusOne();
        List<AgentCounterDTO> agents = agency_agentPOJOList.get(targetAgency.getAgency().getId());
        AgentCounterDTO targetAgent = agents.stream().min(Comparator.comparing(AgentCounterDTO::getPolicyCount))
                .orElseThrow(NoSuchElementException::new);
        targetAgent.plusOne();
        policy.setAgency(targetAgency.getAgency());
        policy.setAssignedAgentUsername(targetAgent.getAgent().getUsername());
        return policy;
    }
}
