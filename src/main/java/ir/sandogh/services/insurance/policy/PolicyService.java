package ir.sandogh.services.insurance.policy;

import ir.sandogh.dto.v1.PageableDTO;
import ir.sandogh.dto.v1.insurance.policy.SupervisorChangePoliciesAgentDTO;
import ir.sandogh.dto.v1.insurance.policy.SupervisorFetchPoliciesDTO;
import ir.sandogh.dto.v1.insurance.policy.UpdatePolicyStateDTO;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.entities.insurance.policy.PolicyState;
import ir.sandogh.repositories.insurance.policy.PolicyRepository;
import ir.sandogh.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PolicyService extends BaseService {

    @Autowired
    private PolicyRepository policyRepository;

    @Autowired
    private PolicyTransactionService policyTransactionService;

    public Iterable<Policy> findAll() {
        return policyRepository.findAll();
    }

    public Policy save(Policy policy) {
        policy.setUsername(getLoggedInUsername());
        policy.setSubmitDate(LocalDateTime.now(ZoneId.systemDefault()));
        policy.setState(PolicyState.SUBMITTED);
        return policyRepository.save(policy);
    }

    public Policy saveNonPaymentPolicySave(Policy policy) {
        policy = policyTransactionService.assignAgent(policy);
        policy.setState(PolicyState.PAID);
        policy.setSubmitDate(LocalDateTime.now(ZoneId.systemDefault()));
        policy.setUsername(getLoggedInUsername());
        return policyRepository.save(policy);
    }

    public Policy updatePolicyState(UpdatePolicyStateDTO dto) {
        Optional<Policy> appOp = policyRepository.findById(dto.getId());
        if (appOp.isPresent()) {
            Policy app = appOp.get();
            if (app.getAssignedAgentUsername().equals(dto.getAgentUsername())) {
                app.setState(dto.getState());
                if (dto.getState().equals(PolicyState.ISSUED)) {
                    app.setIssueDate(LocalDateTime.now(ZoneId.systemDefault()));
                }
                return policyRepository.save(app);
            } else {
                throw new RuntimeException(getErrorMessage("agentNotAssigned"));
            }
        } else {
            throw new RuntimeException(getErrorMessage("policyNotFound"));
        }
    }

    public List<Long> supervisorChangePoliciesAgent(SupervisorChangePoliciesAgentDTO dto) {
        List<Long> result = new ArrayList<>();
        for (Long id : dto.getIds()) {
            Optional<Policy> appOp = policyRepository.findById(id);
            if (appOp.isPresent()) {
                Policy app = appOp.get();
                if (!app.getState().equals(PolicyState.LOCKED)) {
                    app.setAssignedAgentUsername(dto.getTargetAgentUsername());
                    result.add(policyRepository.save(app).getId());
                }
            }
        }
        return result;
    }

    public List<Policy> agentFindPolicies(PageableDTO dto) {
        return policyRepository.findAllByStateAndAssignedAgentUsername(PolicyState.PAID, getLoggedInUsername(), getPageable(dto));
    }

    public List<Policy> supervisorGetApplications(SupervisorFetchPoliciesDTO dto) {
        return policyRepository.findAllByStateAndInsurer(PolicyState.PAID, dto.getInsurer(), getPageable(dto.getPageableDTO()));
    }
}
