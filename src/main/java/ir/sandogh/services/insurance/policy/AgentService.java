package ir.sandogh.services.insurance.policy;

import ir.sandogh.entities.insurance.policy.Agent;
import ir.sandogh.repositories.insurance.policy.AgentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AgentService {
    @Autowired
    private AgentRepository agentRepository;

    public Iterable<Agent> findAll() {
        return agentRepository.findAll();
    }

    public Agent findByUsername(String username) {
        return agentRepository.findByUsername(username);
    }
}
