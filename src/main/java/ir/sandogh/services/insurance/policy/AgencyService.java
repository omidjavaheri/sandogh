package ir.sandogh.services.insurance.policy;

import ir.sandogh.entities.insurance.policy.Agency;
import ir.sandogh.repositories.insurance.policy.AgencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AgencyService {
    @Autowired
    private AgencyRepository agencyRepository;

    public Iterable<Agency> findAll() {
        return agencyRepository.findAll();
    }
}
