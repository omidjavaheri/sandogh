package ir.sandogh.services.insurance;

import ir.sandogh.dto.v1.insurance.payment.AuthorityDTO;
import ir.sandogh.entities.insurance.policy.Policy;
import ir.sandogh.entities.insurance.policy.PolicyTransaction;
import ir.sandogh.partners.payment.com.zarinpal.PaymentGatewayImplementationService;
import ir.sandogh.partners.payment.com.zarinpal.PaymentGatewayImplementationServicePortType;
import ir.sandogh.partners.payment.com.zarinpal.domain.ZarinStatus;
import ir.sandogh.services.BaseService;
import ir.sandogh.services.insurance.policy.PolicyService;
import ir.sandogh.services.insurance.policy.PolicyTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Holder;
import java.io.IOException;

@Service
@Transactional
public class InsurancePaymentService extends BaseService {
    private final String merchantId = "eea9b006-dd10-11e9-b3a6-000c295eb8fc";
    private final String callbackURL = "http://127.0.0.1:9000/v1/insurance/payment/callback";
    private final PaymentGatewayImplementationService serviceImpl = new PaymentGatewayImplementationService();
    private final PaymentGatewayImplementationServicePortType service = serviceImpl.getPaymentGatewayImplementationServicePort();
    @Autowired
    private PolicyTransactionService policyTransactionService;
    @Autowired
    private PolicyService policyService;

    public AuthorityDTO paymentRequest(Policy policy) {
        policy = policyService.save(policy);
        Holder<Integer> status = new Holder<>();
        Holder<String> authority = new Holder<>();
        service.paymentRequest(merchantId, policy.getPrice().intValue(), "insurance", policy.getEmail(), policy.getMobile(), callbackURL, status, authority);
        if (status.value.equals(100) && authority.value.length() == 36) {
            PolicyTransaction policyTransaction = new PolicyTransaction();
            policyTransaction.setPrice(policy.getPrice().intValue());
            policyTransaction.setAuthority(authority.value);
            policyTransaction.setPolicy(policy);
            policyTransactionService.save(policyTransaction);
            return new AuthorityDTO(authority.value);
        } else {
            throw new RuntimeException(getErrorMessage(ZarinStatus.fromCode(status.value).toString()));
        }
    }

    public void callback(HttpServletResponse response, String authority, String status) {
        if ("OK".equals(status)) {
            Long result = policyTransactionService.verifyAfterPayment(service, this.merchantId, authority);
            try {
                if (result != null) {
                    response.sendRedirect("http://sandogh360.ir/insurance/final?status=success&refId=" + result);
                } else {
                    response.sendRedirect("http://sandogh360.ir/insurance/final?status=failed");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                response.sendRedirect("http://sandogh360.ir/insurance/final?status=failed");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
